#!/usr/bin/env python3

import socket, uuid, time, os, sys, datetime
from typing import Callable, Tuple, List

# Загружаем нужные модули из сгенерированных Protobuf'ом описаний по протоколу
import Veresk_common_pb2
import Veresk_RDMP_pb2
import InfoPacket_pb2
import OperationsInstructions_pb2
import Identity_pb2
import FileFragment_pb2

# Здесь лежит словарь, ключ - uuid файла, значение - его имя
uuid2name = {}


def static_vars(**kwargs):
    """
    Декоратор для статических переменных
    :param kwargs:
    :return:
    """

    def decorate(func: Callable) -> Callable:
        """
        Собственно сам декоратор
        :param func: Функция, которую надо декорировать
        :return: Отдекорированная функция со квази-статическими переменными
        """
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func

    return decorate


@static_vars(counter=0)
def get_next_task_id() -> int:
    """
    Возвращает новый идентификатор задания
    :return: Целое число - идентификатор задания
    """
    get_next_task_id.counter += 1
    return get_next_task_id.counter - 1


@static_vars(counter=0)
def get_next_packet_id() -> int:
    """
    Возвращает новый идентификатор пакета
    :return: Целое число - идентификатор пакета
    """
    get_next_packet_id.counter += 1
    #print("Next packet id will be {0}".format(get_next_packet_id.counter - 1))
    return get_next_packet_id.counter - 1


def get_uuid(id: int = -1) -> object:
    """
    Возвращает новый уникальный идентификатор с закодированным в самых старших 4 байтах числом типа int
    :param id: число, которое нужно подставить вместо старших 4 байт в UUID
    :return: объект типа Rdmp.Identity.Uuid
    """
    u = uuid.uuid1()
    if id != -1:
        ub = u.bytes[0:12] + id.to_bytes(4, 'little', signed=False)
    else:
        ub = u.bytes[0:16]

    p1 = int.from_bytes(ub[0:8], 'little')
    p2 = int.from_bytes(ub[8:], 'little')
    uuid_obj = Identity_pb2.Uuid()
    uuid_obj.p1 = p1
    uuid_obj.p2 = p2
    return uuid_obj


def uuid2string(uuid: object) -> str:
    """
    Конвертирует объект типа Rdmp.Uuid в строку
    :param uuid: объект типа Rdmp.Uuid
    :return: строка с шестнадцатиричным представлением uuid
    """
    b1 = uuid.p1.to_bytes(8, 'little', signed=False)
    b2 = uuid.p2.to_bytes(8, 'little', signed=False)

    return b1.hex() + b2.hex()


def uuid2bytes(uuid: object) -> str:
    """
    Конвертирует объект типа Rdmp.Uuid в байтовое представление
    :param uuid: объект типа Rdmp.Uuid
    :return: набор байт
    """
    b1 = uuid.p1.to_bytes(8, 'little', signed=False)
    b2 = uuid.p2.to_bytes(8, 'little', signed=False)

    return b1 + b2


def bytes2uuid(s: bytes) -> object:
    """
    Конвертирует строку в объект Rdmp.Uuid
    :param s: байты
    :return: объект типа Rdmp.Uuid
    """
    p1 = int.from_bytes(s[0:8], 'little')
    p2 = int.from_bytes(s[8:], 'little')
    uuid_obj = Identity_pb2.Uuid()
    uuid_obj.p1 = p1
    uuid_obj.p2 = p2
    return uuid_obj


def ts2str(ts: int) -> str:
    """
    Конвертирует временную метку (в миллисекундах!) в строку с численным представлением и датой/временем.
    Время приводится для часового пояса UTC!
    :param ts: оьъект типа int
    :return: строка с численным представлением временной отметки и соответствующей ей датой/временем
    """
    return "{} ms ('{}')".format(ts, datetime.datetime.fromtimestamp(ts/1000, datetime.timezone.utc).strftime("%c"))


def create_notification_packet(id: int) -> object:
    """
    Создает notification-пакет
    :param id: числовой идентификатор пакета
    :return: Объект типа InfoPacket, содержащий внутри notification-пакет
    """
    ip = InfoPacket_pb2.InfoPacket()
    ip.type = InfoPacket_pb2.InfoPacket.Notification
    ip.id = id

    return ip


def send_notification_packet(s: object, np: object) -> None:
    """
    Посылает notification-пакет по сокету s
    :param s: объект типа 'сокет'
    :param np: объект типа FileRequest, упакованный в InfoPacket
    :return: None
    """
    np_bytes = np.SerializeToString()

    s.send(int(len(np_bytes)).to_bytes(4, 'little'))
    s.send(int(0).to_bytes(8, 'little'))
    s.send(int(0).to_bytes(2, 'little'))
    s.send(np_bytes)

    #print("Send notification packet")


def create_info_packet_with_tasks(task_vec: List[Veresk_common_pb2.Task]) -> (object, object):
    """
    Создает объект InfoPacket, внутри которого упакован запрос на создание группы заданий,
    перечисленных в task_vec
    :param task_vec: List[Veresk_common_pb2.Task] - группа заданий, которую нужно упаковать
    :return: (object, object) - кортеж из объекта типа InfoPacket и объекта Uuid,
    который относится к отдельному OperationsInstructions и может быть использован
    в дальнейшем для удаления группы заданий
    """

    oi = OperationsInstructions_pb2.OperationsInstructions()
    oi.id.CopyFrom(get_uuid())
    oi.type = OperationsInstructions_pb2.OperationsInstructions.CreateOrUpdate
    oi.title = "Группа заданий №1"

    for vt in task_vec:
        roi = OperationsInstructions_pb2.Roi()
        roi.id.CopyFrom(get_uuid())

        roi_ins = oi.instructions.add()
        roi_ins.roi.CopyFrom(roi)

        ot = roi_ins.operationTasks.add()
        ot.specificTaskType = OperationsInstructions_pb2.OperationTask.Veresk
        ot.specificTask = vt.SerializeToString()

    ip = InfoPacket_pb2.InfoPacket()
    ip.type = InfoPacket_pb2.InfoPacket.OperationsInstructions
    ip.id = get_next_packet_id()
    ip.data = oi.SerializeToString()

    return ip, oi.id


def create_common_veresk_task(name: str, is_all_dets: bool) -> Veresk_common_pb2.Task:
    """
    Создает новое задание для Вереска для поиска сигналов с фазовой манипуляцией, а также ALE-2G
    :param name: str - имя нового задания
    :param is_all_dets: bool - флаг, требующий включить в задание все обнаружители
    :return Veresk_common_pb2.Task: Объект типа Veresk_common_pb2.Task
    """

    # Для начала создаем объект задания и заполняем поля его имени и типа (поиск/наблюдение)
    vt = Veresk_common_pb2.Task()
    vt.task_name = name
    vt.task_type = Veresk_common_pb2.Task.TT_SEARCH
    # Задание на мониторинг (отдельных частот) нужно создавать вот так:
    #vt.task_type = Veresk_common_pb2.Task.TT_MONITORING

    # Теперь заполним классы сигналов, которые нужно обнаруживать в задании
    vt.sig_classes.extend([Veresk_common_pb2.SC_ALE2G,
                           Veresk_common_pb2.SC_ALE3G,
                           Veresk_common_pb2.SC_ST4529,
                           Veresk_common_pb2.SC_MIL110A,
                           Veresk_common_pb2.SC_MIL110B_APPC,
                           Veresk_common_pb2.SC_MIL110C_APPD,
                           Veresk_common_pb2.SC_DC7200,
                           Veresk_common_pb2.SC_LINK22,
                           Veresk_common_pb2.SC_B162
                           ])

    # Включим сомнительный с точки зрения необходимости обнаружитель ST4285, если включен флаг.
    if is_all_dets == True:
        vt.sig_classes.extend([Veresk_common_pb2.SC_ST4285])

    # Не надо нам посылать запросы на пеленгатор, он и так через БД увидит нашу работу
    # и подпишет пеленги
    vt.request_directions = False
    vt.request_locations = False

    # Использовать оперативный откат - хорошая идея для заданий на поиск
    vt.use_fast_rollback = True

    # Добавим диапазон частот 1.5 ... 30 МГц в задание с шагом в 2.5 кГц
    default_freq_range = vt.freq_ranges.add()
    default_freq_range.start_freq = 1500000
    default_freq_range.end_freq = 30000000
    default_freq_range.delta = 2500

    # Можно вот так добавить отдельные частоты (например, в задание для мониторинга):
    #vt.freqs.append(9795000)
    #vt.freqs.append(7845000)

    # Запросим, например, 500 каналов для этого задания
    vt.requested_channels = 500

    return vt


def create_dummy_mon_task() -> Veresk_common_pb2.Task:
    """
    Создает новое задание для Вереска с предопределенными частотами для мониторинга
    :return Veresk_common_pb2.Task: Объект типа Veresk_common_pb2.Task
    """

    vt = Veresk_common_pb2.Task()
    vt.task_name = "DUMMY"
    vt.task_type = Veresk_common_pb2.Task.TT_MONITORING

    vt.sig_classes.extend([Veresk_common_pb2.SC_MIL110A, Veresk_common_pb2.SC_MIL110B_APPC,
                           Veresk_common_pb2.SC_MIL110C_APPD, Veresk_common_pb2.SC_ALE3G])
    vt.request_directions = False
    vt.request_locations = False

    gps_freqs = [10100, 4995, 11038]

    for freq in gps_freqs:
        vt.freqs.append(int((freq) * 1000))

    return vt


def create_del_task_request(task_uuid: object) -> object:
    """
    Удаляет задание или группу заданий Вереска
    :param task_uuid: object - уникальный идентификатор задания, которое нужно удалить
    :return object: Объект типа InfoPacket, содержащий внутри запрос на удаление задания
    """
    oi = OperationsInstructions_pb2.OperationsInstructions()
    oi.id.CopyFrom(task_uuid)
    oi.type = OperationsInstructions_pb2.OperationsInstructions.Cancel

    ip = InfoPacket_pb2.InfoPacket()
    ip.type = InfoPacket_pb2.InfoPacket.OperationsInstructions
    ip.id = get_next_packet_id()
    ip.data = oi.SerializeToString()

    return ip


def get_radio_user_category(cat: object) -> str:
    """
    Конвертирует в строку категорию абонента
    :param cat: категория абонента
    :return: строка с названием категории
    """
    if cat == Veresk_common_pb2.RUC_GENERIC:
        return "Generic category"
    elif cat == Veresk_common_pb2.RUC_ALE2G:
        return "ALE2G callsign"
    elif cat == Veresk_common_pb2.RUC_PRO:
        return "ST5066 callsign"
    elif cat == Veresk_common_pb2.RUC_IP:
        return "IP address"
    elif cat == Veresk_common_pb2.RUC_EMAIL_ADDR:
        return "Email address"
    elif cat == Veresk_common_pb2.RUC_EMAIL_NAME:
        return "Email name"
    else:
        return "N/A"


def parse_radio_user_list(rul: list) -> None:
    """
    Разбирает список с именами абонента
    :param rul: список с именами абонента
    :return: None
    """
    if len(rul) == 0:
        return

    for ru in rul:
        print("        { ")
        print("            .name: ", ru.name)
        print("            .id: ", ru.id)
        print("            .category: ", get_radio_user_category(ru.category))
        print("        }, ")


def parse_antenna(a: object) -> None:
    """
    Разбирает объект типа Antenna
    :param a: объект типа Antenna
    :return: None
    """
    print("        .prc.antenna: ")
    print("              .name: ", a.name)
    print("              .id: ", a.id)
    print("              .is_unidirectional: ", a.is_unidirectional)
    print("              .direction: ", a.direction)


def parse_processed_files(s: object, pf: list) -> None:
    """
    Разбирает список обработанных файлов и отсылает file request для каждого из них
    :param s: сокет
    :param pf: список обработанных файлов (объектов типа ProcessedFileMetadata)
    :return: None
    """
    print("            .processed_files: [")
    for pfm in pf:
        s_uuid = uuid2string(pfm.file_uuid)
        print("              .data_file_name: ", pfm.data_file_name)
        print("              .data_type: ", pfm.data_type)
        print("              .data_type_desc: ", pfm.data_type_desc)
        print("              .sub_data: ", pfm.sub_data)
        print("              .db_blob_id: ", pfm.db_blob_id)
        print("              .file_uuid: ", s_uuid)
        print("              .parsed_data_category: ", pfm.parsed_data_category)

        if pfm.parsed_data_category == Veresk_common_pb2.PDC_HFDL:
            for pd in pfm.parsed_data:
                parse_hfdl(pd)
        elif pfm.parsed_data_category == Veresk_common_pb2.PDC_GMDSS_DSC:
            for pd in pfm.parsed_data:
                parse_gmdss(pd)
        elif pfm.parsed_data_category == Veresk_common_pb2.PDC_NAVTEX:
            for pd in pfm.parsed_data:
                parse_navtex(pd)
        elif pfm.parsed_data_category == Veresk_common_pb2.PDC_B162:
            for pd in pfm.parsed_data:
                parse_b162(pd)
        elif pfm.parsed_data_category == Veresk_common_pb2.PDC_GPS:
            for pd in pfm.parsed_data:
                parse_gps(pd)

        # Заполним словарь, чтобы потом понять, как uuid файла соотносится с его именем
        uuid2name[s_uuid] = pfm.data_file_name
        send_file_request(s, create_file_request(pfm.file_uuid))
    print("            ]")


def parse_hfdl(b: bytes) -> None:
    """
    Разбирает объект типа ParsedHFDL
    :param b: объект типа ParsedHFDL в двоичном виде
    :return: None
    """
    hfdl = Veresk_common_pb2.ParsedHFDL()
    hfdl.ParseFromString(b)

    print("                HFDL object:")
    print("                  .crc_check: ", hfdl.crc_check)
    print("                  .inf_rate: ", hfdl.inf_rate)
    print("                  .is_long_packet: ", hfdl.is_long_packet)
    print("                  .freq_offset: ", hfdl.freq_offset)
    print("                  .session_packet_offset: ", hfdl.session_packet_offset)
    print("                  .packet_received_timestamp: ", ts2str(hfdl.packet_received_timestamp))
    print("                  .ground station: ", hfdl.groundStation)
    print("                  .airplane: ", hfdl.airplane)
    print("                  .lpdu_count: ", hfdl.lpdu_count)
    print("                  LPDU: [")

    for lpdu in hfdl.lpdu_array:
        print("                    .crc_check: ", lpdu.crc_check)
        print("                    .type: ", lpdu.type)
        print("                    .flag: ", lpdu.flag)
        print("                    .message: ", lpdu.message)
        print("                    .flight_id: ", lpdu.flight_id)
        print("                    .icao_id: ", lpdu.icao_id)
        print("                    .coords: ")
        print("                      .latitude: ", lpdu.coords.geo.latitude)
        print("                      .longitude: ", lpdu.coords.geo.longitude)
        print("                    .in_packet_timestamp: ", ts2str(lpdu.in_packet_timestamp))
        print("                    .airplane: ", lpdu.airplane)
    print("                  ]")


def parse_gmdss(b: bytes) -> None:
    """
    Разбирает объект типа ParsedGMDSSDSC
    :param b: объект типа ParsedGMDSSDSC в двоичном виде
    :return: None
    """
    gmdss = Veresk_common_pb2.ParsedGMDSSDSC()
    gmdss.ParseFromString(b)

    print("                GMDSS DSC object:")
    print("                  .crc_check: ", gmdss.crc_check)
    print("                  .session_packet_offset: ", gmdss.session_packet_offset)
    print("                  .packet_received_timestamp: ", ts2str(gmdss.packet_received_timestamp))

    dsc = gmdss.gmdss_dsc

    if dsc.HasField("corrValue"):
        print("                .corrValue:", dsc.corrValue)
    if dsc.HasField("receivedTime"):
        print("                .receivedTime.dateTime", dsc.receivedTime.dateTime)
        print("                .receivedTime.nanoSecs", dsc.receivedTime.nanoSecs)

    if dsc.HasField("msgFormat"):
        print("                .msgFormat:", dsc.msgFormat)
    if dsc.HasField("msgCategory"):
        print("                .msgCategory:", dsc.msgCategory)
    if dsc.HasField("msgType"):
        print("                .msgType:", dsc.msgType)
    if dsc.HasField("distrType"):
        print("                .distrType:", dsc.distrType)
    if dsc.HasField("subSeqComm"):
        print("                .subSeqComm:", dsc.subSeqComm)
    if dsc.HasField("fstCmd"):
        print("                .fstCmd:", dsc.fstCmd)
    if dsc.HasField("sndCmd"):
        print("                .sndCmd:", dsc.sndCmd)
    if dsc.HasField("selfID"):
        print("                .selfID:", dsc.selfID.shipID)
    if dsc.HasField("destID"):
        print("                .destID:", dsc.destID.shipID)
    if dsc.HasField("distrID"):
        print("                .distrID:", dsc.distrID.shipID)
    if dsc.HasField("distrCoords"):
        dc = dsc.distrCoords
        p = dc.coords
        geo = p.geo
        print("                .distrCoords...latitude:", geo.latitude)
        print("                .distrCoords...longitude:", geo.longitude)
        print("                .distrCoords...altitude:", geo.altitude)
        print("                .distrCoords.timeHours:", dc.timeHours)
        print("                .distrCoords.timeMinutes:", dc.timeMinutes)
    if dsc.HasField("selfCoords"):
        dc = dsc.selfCoords
        p = dc.coords
        geo = p.geo
        print("                .selfCoords...latitude:", geo.latitude)
        print("                .selfCoords...longitude:", geo.longitude)
        print("                .selfCoords...altitude:", geo.altitude)
        print("                .selfCoords.timeHours:", dc.timeHours)
        print("                .selfCoords.timeMinutes:", dc.timeMinutes)
    if len(dsc.geoAreaCoords) > 0:
        for p in dsc.geoAreaCoords:
            geo = p.geo
            print("                .geoAreaCoords.latitude:", geo.latitude)
            print("                .geoAreaCoords.longitude:", geo.longitude)
            print("                .geoAreaCoords.altitude:", geo.altitude)
    if dsc.HasField("freq1"):
        print("                .freq1:", dsc.freq1)
    if dsc.HasField("freq2"):
        print("                .freq2:", dsc.freq2)
    if dsc.HasField("chan1"):
        print("                .chan1:", dsc.chan1)
    if dsc.HasField("chan2"):
        print("                .chan2:", dsc.chan2)
    if dsc.HasField("callDuration"):
        print("                .callDuration:", dsc.callDuration)
    if dsc.HasField("phoneNumber"):
        print("                .phoneNumber:", dsc.phoneNumber)
    print("                End of GMDSS DSC object")


def parse_navtex(b: bytes) -> None:
    """
    Разбирает объект типа ParsedNAVTEX
    :param b: объект типа ParsedNAVTEX в двоичном виде
    :return: None
    """
    nt = Veresk_common_pb2.ParsedNAVTEX()
    nt.ParseFromString(b)

    print("                NAVTEX object:")
    print("                  .msg_id: ", nt.msg_id)
    print("                  .station_id: ", nt.station_id)
    print("                  .msg_type: ", nt.msg_type)
    print("                  .char_msg_type: ", nt.char_msg_type)
    print("                  .msg_number: ", nt.msg_number)
    print("                  .message_text: ", nt.message_text)

    print("                  .coords: ")
    for c in nt.coords:
        print("                    .geo.latitude: ", c.geo.latitude)
        print("                    .geo.longitude: ", c.geo.longitude)
        print("                    .geo.altitude: ", c.geo.altitude)
    print("                End of NAVTEX object")


def parse_b162(b: bytes) -> None:
    """
    Разбирает объект типа ParsedB162
    :param b: объект типа ParsedB162 в двоичном виде
    :return: None
    """
    b162 = Veresk_common_pb2.ParsedB162()
    b162.ParseFromString(b)

    print("                B162 object:")
    if b162.HasField("msg_type1"):
        bt1 = b162.msg_type1
        print("                  .msg_type1:")
        if bt1.HasField("station_id"):
            print("                    .station_id: ", bt1.station_id)
        if bt1.HasField("time"):
            print("                    .time: ", bt1.time)
        if bt1.HasField("primary_id"):
            print("                    .primary_id: ", bt1.primary_id)
        if bt1.HasField("secondary_id"):
            print("                    .secondary_id: ", bt1.secondary_id)
        if bt1.HasField("coords"):
            print("                    .coords: ")
            print("                      .latitude: ", bt1.coords.geo.latitude)
            print("                      .longitude: ", bt1.coords.geo.longitude)
        if bt1.HasField("course"):
            print("                    .course: ", bt1.course)
        if bt1.HasField("velocity"):
            print("                    .velocity: ", bt1.velocity)
    elif b162.HasField("msg_type2"):
        bt2 = b162.msg_type2
        print("                  .msg_type2:")
        if bt2.HasField("station_id"):
            print("                    .station_id: ", bt2.station_id)
        if bt2.HasField("time"):
            print("                    .time: ", bt2.time)
        if bt2.HasField("primary_id"):
            print("                    .primary_id: ", bt2.primary_id)
        if bt2.HasField("secondary_id"):
            print("                    .secondary_id: ", bt2.secondary_id)
        if bt2.HasField("course"):
            print("                    .course: ", bt2.course)
        if bt2.HasField("height"):
            print("                    .height: ", bt2.height)
        if bt2.HasField("velocity"):
            print("                    .velocity: ", bt2.velocity)
        if bt2.HasField("azimuth"):
            print("                    .azimuth: ", bt2.azimuth)
        if bt2.HasField("range"):
            print("                    .range: ", bt2.range)
    else:
        print("                B162 object have neither msg_type1 nor msg_type2")


def parse_gps(b: bytes) -> None:
    """
    Разбирает объект типа ParsedGPS
    :param b: объект типа ParsedGPS в двоичном виде
    :return: None
    """
    gps = Veresk_common_pb2.ParsedGPS()
    gps.ParseFromString(b)

    print("                GPS object:")
    print("                  .from: ", gps.from_id)
    print("                  .to: ", gps.to_id)
    print("                  .coords:")
    print("                    .latitude: ", gps.coords.geo.latitude)
    print("                    .longitude: ", gps.coords.geo.longitude)


def parse_file_fragment(ff: object, payload: bytes) -> None:
    """
    Разбирает объект типа FileFragment
    :param ff: объект типа FileFragment
    :return: None
    """
    print("FileFragment.uuid: {}".format(uuid2string(ff.uuid)))
    print("            .name: ", ff.filename)
    print("            .length: ", ff.length)
    print("            .offset: ", ff.offset)
    print("            .checksum: ", ff.checksum)
    print("Payload size: ", len(payload))

    try:
      os.stat("records")
    except:
      os.mkdir("records")

    frag_file = open("records/" + uuid2string(ff.uuid) + "_" + ff.filename, 'ab')
    frag_file.write(payload)
    frag_file.close()


def parse_veresk_result(s: object, vr: object) -> None:
    """
    Разбирает на части объект типа VereskResult
    :param s: сокет
    :param vr: объект типа VereskResult
    :return: None
    """
    print("VereskResult id: {}".format(uuid2string(vr.id)))
    print("        .prc.start_timestamp: ", ts2str(vr.proc_session_info.start_timestamp))
    print("        .prc.end_timestamp: ", ts2str(vr.proc_session_info.end_timestamp))

    print("        .prc.from_list: [")
    parse_radio_user_list(vr.proc_session_info.from_list)
    print("        ]")

    print("        .prc.to_list: [")
    parse_radio_user_list(vr.proc_session_info.to_list)
    print("        ]")

    print("        .prc.radio_net.name: ", vr.proc_session_info.radio_net.name)
    print("        .prc.radio_net.id: ", vr.proc_session_info.radio_net.id)
    print("        .prc.freq: ", vr.proc_session_info.freq)
    print("        .prc.sig_classes_list: [")
    for sc in vr.proc_session_info.sig_classes_list:
        print("          signal_class: {}, signal_class_name: {}".format(str(sc.signal_class), sc.signal_class_name))
    print("        ]")
    print("        .prc.recvr.name: ", vr.proc_session_info.recvr.name)
    print("        .prc.recvr.max_channels: ", vr.proc_session_info.recvr.max_channels)
    print("        .prc.recvr_channel: ", vr.proc_session_info.recvr_channel)
    parse_antenna(vr.proc_session_info.antenna)
    parse_processed_files(s, vr.proc_session_info.processed_files)
    print("        .prc.db_session_id: ", vr.proc_session_info.db_session_id)


def parse_event_opened_session(vos: object) -> None:
    """
    Разбирает на части объект типа VereskOpenedSession
    :param vos: объект типа VereskOpenedSession
    :return:
    """
    print("          .start_timestamp: ", vos.start_timestamp, " (", ts2str(vos.start_timestamp), ")")
    print("          .recvr_freq: ", vos.recvr_freq)
    print("          .detect_freq: ", vos.detect_freq)
    print("          .sess_freq: ", vos.sess_freq)
    print("          .sig_class.signal_class: ", str(vos.sig_class.signal_class), ", sig_class.signal_class_name: ", str(vos.sig_class.signal_class_name))
    print("          .is_lsb: ", vos.is_lsb)
    print("          .baud_rate: ", vos.baud_rate)
    print("          .cred_measure: ", vos.cred_measure)
    print("          .recvr.name: ", vos.recvr.name)
    #print("            .recvr.max_channels: ", vos.recvr.max_channels)
    print("          .recvr_channel: ", vos.recvr_channel)
    parse_antenna(vos.antenna)
    print("          .db_session_id: ", vos.db_session_id)


def parse_event_closed_session(vcs: object) -> None:
    """
    Разбирает на части объект типа VereskClosedSession
    :param vcs: объект типа VereskClosedSession
    :return:
    """
    print("          .start_timestamp: ", vcs.start_timestamp, " (", ts2str(vcs.start_timestamp), ")")
    print("          .end_timestamp: ", vcs.end_timestamp, " (", ts2str(vcs.end_timestamp), ")")
    print("          .sess_freq: ", vcs.sess_freq)
    print("          .sig_classes_list: [")
    for sc in vcs.sig_classes_list:
        print("            signal_class: {}, signal_class_name: {}".format(str(sc.signal_class), sc.signal_class_name))
    print("          ]")
    print("          .is_lsb: ", vcs.is_lsb)
    print("          .baud_rate: ", vcs.baud_rate)
    print("          .recvr.name: ", vcs.recvr.name)
    #print("            .recvr.max_channels: ", vcs.recvr.max_channels)
    print("          .recvr_channel: ", vcs.recvr_channel)
    parse_antenna(vcs.antenna)
    print("          .db_session_id: ", vcs.db_session_id)


def parse_event_signal_deteced(vsd: object) -> None:
    """
    Разбирает на части объект типа VereskSignalDetected
    :param vsd: объект типа VereskSignalDetected
    :return:
    """
    print("          .detect_timestamp: ", vsd.detect_timestamp, " (", ts2str(vsd.start_timestamp), ")")
    print("          .lost_timestamp: ", vsd.lost_timestamp, " (", ts2str(vsd.lost_timestamp), ")")
    print("          .recvr_freq: ", vsd.recvr_freq)
    print("          .detect_freq: ", vsd.detect_freq)
    print("          .sig_class.signal_class: ", str(vsd.sig_class.signal_class), ", .sig_class.signal_class_name: ", str(vsd.sig_class.signal_class_name))
    print("          .is_lsb: ", vos.is_lsb)
    print("          .baud_rate: ", vos.baud_rate)
    print("          .cred_measure: ", vos.cred_measure)
    print("          .recvr.name: ", vos.recvr.name)
    #print("            .recvr.max_channels: ", vos.recvr.max_channels)
    print("          .recvr_channel: ", vos.recvr_channel)
    parse_antenna(vos.antenna)
    print("          .db_session_id: ", vos.db_session_id)


def parse_veresk_event(s: object, ve: object) -> None:
    """
    Разбирает на части объект типа VereskEvent
    :param s: сокет
    :param ve: объект типа VereskEvent
    :return: None
    """
    print("VereskEvent task id: {}".format(uuid2string(ve.id)))
    print("        .event_type: ", ve.event_type)

    if ve.event_type == Veresk_common_pb2.VET_OPENED_SESSION:
        print("        .open_sess: ")
        parse_event_opened_session(ve.open_sess)
    elif ve.event_type == Veresk_common_pb2.VET_CLOSED_SESSION:
        print("        .close_sess: ")
        parse_event_closed_session(ve.close_sess)
    elif ve.event_type == Veresk_common_pb2.VET_SIGNAL_DETECTED:
        print("        .sig_det: ")
        parse_event_signal_detected(ve.sig_det)
    else:
        print("Unknown VereskEvent type! Cannot parse it!")


def parse_info_packet(s: object, ip: object, payload: bytes) -> None:
    """
    Разбирает на части InfoPacket
    :param s: object - сокет
    :param ip: object - объект типа InfoPacket
    :param payload: bytes - двоичное вложение, которое идет сразу после InfoPacket
    :return: None
    """

    # Здесь пришедший пакет InfoPacket анализируется на предмет того,
    # что у него внутри, и для дальнейшего разбора содержимого вызывается
    # соответствуюшая функция.
    
    # Поле type определяет, какой подтип пакета к нам приехал по сети
    if ip.type == InfoPacket_pb2.InfoPacket.VereskResult:
        # VereskResult - результаты обработки записи сигнала
        print("InfoPacket has type VereskResult")
        vr = Veresk_RDMP_pb2.VereskResult()
        vr.ParseFromString(ip.data)
        parse_veresk_result(s, vr)
    elif ip.type == InfoPacket_pb2.InfoPacket.VereskEvent:
        # VereskEvent - уведомление о начале или завершении сеанса регистрации сигнала,
        # либо просто уведомление о срабатывании обнаружителя (возможно, еще не реализовано)
        print("InfoPacket has type VereskEvent")
        ve = Veresk_RDMP_pb2.VereskEvent()
        ve.ParseFromString(ip.data)
        parse_veresk_event(s, ve)
    elif ip.type == InfoPacket_pb2.InfoPacket.Notification:
        # Notification - уведомление о том, что предыдущий отправленный пакет был принят
        # на стороне Вереска
        print("InfoPacket has type 'Notification', id: ", ip.id)
    elif ip.type == InfoPacket_pb2.InfoPacket.FileRequest:
        # FileRequest - запрос на получение файла. Такой запрос отсылается в сторону Вереска,
        # поэтому вряд ли строка ниже когда-нибудь сработает.
        print("InfoPacket has type 'FileRequest', id: ", ip.id)
    elif ip.type == InfoPacket_pb2.InfoPacket.FileFragment:
        # FileFragment - ответ с каким-либо куском файла, запрошенного ранее у Вереска
        # через FileRequest. Если файл был мелкий, то вполне возможно, что в payload лежит
        # он весь, и поэтому присылать другие FileFragment Вереск уже не будет.
        print("InfoPacket has type 'FileFragment', id: ", ip.id)
        ff = FileFragment_pb2.FileFragment()
        ff.ParseFromString(ip.data)
        parse_file_fragment(ff, payload)
        # Очень важно отослать уведомление об успешном приеме FileFragment
        send_notification_packet(s, create_notification_packet(ip.id))
    elif ip.type == InfoPacket_pb2.InfoPacket.OperationsInstructionsReport:
        # OperationsInstructionsReport - отчет о результате выполнения задания
        print("InfoPacket has type 'OperationsInstructionsReport', id: ", ip.id)
        oir = OperationsInstructions_pb2.OperationsInstructionsReport()
        oir.ParseFromString(ip.data)
        print("Dump of OperationsInstructionsReport: ", oir)


def send_add_task_request(s: object, atr: object) -> None:
    """
    Посылает запрос на добавление задания по сокету s
    :param s: объект типа 'сокет'
    :param atr: объект типа 'AddTaskRequest'
    :return: None
    """
    add_task_packet_bytes = atr.SerializeToString()

    s.send(int(len(add_task_packet_bytes)).to_bytes(4, 'little'))
    s.send(int(0).to_bytes(8, 'little'))
    s.send(int(0).to_bytes(2, 'little'))
    s.send(add_task_packet_bytes)

    print("Send add task request with length: ", len(add_task_packet_bytes))


def send_del_task_request(s: object, dtr: object) -> None:
    """
    Посылает запрос на удаление задания по сокету s
    :param s: объект типа 'сокет'
    :param dtr: объект типа 'AddTaskRequest'
    :return: None
    """
    del_task_packet_bytes = dtr.SerializeToString()

    s.send(int(len(del_task_packet_bytes)).to_bytes(4, 'little'))
    s.send(int(0).to_bytes(8, 'little'))
    s.send(int(0).to_bytes(2, 'little'))
    s.send(del_task_packet_bytes)

    print("Send del task request with length: ", len(del_task_packet_bytes))


def create_file_request(file_uuid: object) -> object:
    """
    Создает объект типа FileRequest
    :param file_uuid: объект - идентификатор файла
    :return: объект типа FileRequest, упакованный внутрь объекта InfoPacket
    """
    print("Creating file request with uuid {}...".format(uuid2string(file_uuid)))

    fr = FileFragment_pb2.FileRequest()
    fr.uuid.CopyFrom(file_uuid)
    fr.requestType = FileFragment_pb2.FileRequest.Request
    fr.maxFragmentSize = 524288

    ip = InfoPacket_pb2.InfoPacket()
    ip.type = InfoPacket_pb2.InfoPacket.FileRequest
    ip.id = get_next_packet_id()
    ip.data = fr.SerializeToString()

    return ip


def send_file_request(s: object, fr: object) -> None:
    """
    Посылает запрос на получение файла по сокету s
    :param s: объект типа 'сокет'
    :param fr: объект типа FileRequest, упакованный в InfoPacket
    :return: None
    """
    fr_packet_bytes = fr.SerializeToString()

    s.send(int(len(fr_packet_bytes)).to_bytes(4, 'little'))
    s.send(int(0).to_bytes(8, 'little'))
    s.send(int(0).to_bytes(2, 'little'))
    s.send(fr_packet_bytes)

    print("Send file request with length: ", len(fr_packet_bytes))


@static_vars(read_buffer=bytes())
def read_bytes(s : object, n : int) -> bytes:
    """
    Читает из соединения s ровно n байт и возвращает в виде массива bytes
    :param s: Сокет
    :param n: Число байт для прочтения из соединения
    :return: Объект bytes размером n
    """
    if len(read_bytes.read_buffer) >= n:
        bytes_read = read_bytes.read_buffer[0:n]
        read_bytes.read_buffer = read_bytes.read_buffer[n:]
        return bytes_read
    else:
        while len(read_bytes.read_buffer) < n:
            read_bytes.read_buffer += s.recv(n - len(read_bytes.read_buffer))
            if not read_bytes.read_buffer:
                break
        if not read_bytes.read_buffer:
            print("Error reading bytes from connection, current buffer size: {0}, trying to read {1} bytes, "
                  "while trying to achieve read buffer of size {2}".format(len(read_bytes.read_buffer),
                                                                           n - len(read_bytes.read_buffer),
                                                                           n))
            return bytes()
        else:
            bytes_read = read_bytes.read_buffer[0:n]
            read_bytes.read_buffer = read_bytes.read_buffer[n:]
            return bytes_read


def usage():
    print("Usage: {} [command [host-to-connect [TCP-port-to-connect]]]".format(sys.argv[0]))
    print("Command should be:")
    print("  listen     - to listen, parse and print incoming data        [DEFAULT]")
    print("  addtask    - to create sample task and listen for, parse, print incoming data")
    print("  addtaskall - to create sample task with all available detectors and listen for,")
    print("               parse, print incoming data")
    print("  deltask    - to delete that sample task and just exit")
    print("host-to-connect:     should be IP or DNS of host to connect [DEFAULT: 127.0.0.1]")
    print("TCP-port-to-connect: should be TCP port number to connect   [DEFAULT: 32133]")


def main():
    # начало основной программы
    all_cmds = [ "listen", "addtask", "addtaskall", "deltask" ]

    # значения по умолчанию
    port_num = 32133
    cmd = "listen"
    host_addr = "127.0.0.1"

    # Здесь начинается работа программы

    # Сначала определим количество параметров командной строки
    # и прочитаем их все
    if len(sys.argv) == 4:
        cmd = sys.argv[1]
        host_addr = sys.argv[2]
        port_num = int(sys.argv[3])
    elif len(sys.argv) == 3:
        cmd = sys.argv[1]
        host_addr = sys.argv[2]
    elif len(sys.argv) == 2:
        cmd = sys.argv[1]

    if not cmd in all_cmds:
        if cmd != "help":
            print("Unknown command: {}".format(cmd))
        usage()
        sys.exit(1)

    print("Command to do will be '{}', host: {}, port: {}".format(cmd, host_addr, port_num))

    try:
        print("Connecting to host {} port {}...".format(host_addr, port_num))
        s = socket.create_connection((host_addr, port_num))
        print("Connected.")

        if cmd == "addtask" or cmd == "addtaskall":
            # Тут всё просто - создаем задание и отправляем его на Вереск.
            # Заданий может быть несколько, поэтому мы их будем добавлять в список
            task_vec = list()
            task_vec.append(create_common_veresk_task("Задание от " + time.strftime("%d-%m-%y %H:%M:%S"),
                            cmd == "addtaskall"))
            (ip, task_uuid) = create_info_packet_with_tasks(task_vec)
            send_add_task_request(s, ip)

            # Записываем uuid группы заданий в файл, чтобы потом можно было
            # прочитать его, когда нам понадобится её удалить.
            with open("task_uuid.txt", "wb") as f:
                f.write(uuid2bytes(task_uuid))

        # Для команд addtask и listen входим в цикл чтения и парсинга данных по TCP-соединению.
        # Цикл будет продолжаться, пока пользователь не остановит скрипт, или пока соединение не закроется.
        if cmd == "addtask" or cmd == "addtaskall" or cmd == "listen":
            while True:
                # Заголовок в протоколе RDMP всегда имеет значение 14 байт.
                rdmp_header_size = 14

                # Читаем заголовок
                rdmp_header = read_bytes(s, rdmp_header_size)
                if not rdmp_header:
                    print("Empty RDMP header was received, breaking")
                    break

                # Получаем 4-байтовое значение размера пакета InfoPacket (основной тип пакета в протоколе RDMP)
                info_packet_size = int.from_bytes(rdmp_header[0:4], 'little')
                # Получаем значение размера payload (нагрузки), которая идет после InfoPacket
                payload_size = int.from_bytes(rdmp_header[4:12], 'little')
                # Получаем двухбайтовое значение версии прото
                proto_version = int.from_bytes(rdmp_header[12:14], 'little')

                print("Received RDMP header, infopacket size: {0}, payload_size: {1}, proto_version: {2}"
                      .format(info_packet_size, payload_size, proto_version))

                # Читаем сам InfoPacket
                info_packet_bytes = read_bytes(s, info_packet_size)
                if not info_packet_bytes:
                    print("Empty info packet was received, breaking")
                    break

                print("Actual received InfoPacket size:{0}".format(len(info_packet_bytes)))

                # Создаем пустой объект InfoPacket и заполняем его из принятых по сети данных
                info_packet = InfoPacket_pb2.InfoPacket()
                info_packet.ParseFromString(info_packet_bytes)

                # Теперь читаем нагрузку (payload)
                payload_bytes = bytes()

                if payload_size > 0:
                    payload_bytes = read_bytes(s, payload_size)
                    if not payload_bytes:
                        print("Empty payload was received, breaking")
                        break
                    print("Actual payload size:{0}".format(len(payload_bytes)))

                # Парсим принятый InfoPacket, и в зависимости от того, что нам пришло, вызываем нужную функцию
                # для разбора того, что пришло
                parse_info_packet(s, info_packet, payload_bytes)
        elif cmd == "deltask":
            # Здесь будем посылать запрос на удаление задания
            # Сначала прочитаем из файла UUID ранее созданного задания
            with open("task_uuid.txt", "rb") as f:
                task_uuid = bytes2uuid(f.read())
                # Удаляем файл
            os.remove("task_uuid.txt")

            print("Sending del task request")

            # Создаем и отсылаем по сети запрос на удаление задания
            del_task_packet = create_del_task_request(task_uuid)
            send_del_task_request(s, del_task_packet)

            print("Sending del task request is complete")

            # Вереск может упасть, если закрыть соединение слишком быстро после отправки задания :-)
            # Я вроде бы починил это уже, но пусть тут будет sleep на секунду
            time.sleep(1)

        # Закрываем соединение
        s.close()
        print("Connection was closed.")

    except ConnectionRefusedError as e:
        print("Error: ", e)
        return 1

    return 0


# Здесь начинается программа
ret_code = main()
exit(ret_code)
