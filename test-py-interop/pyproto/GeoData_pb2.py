# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: GeoData.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='GeoData.proto',
  package='Rdmp',
  syntax='proto2',
  serialized_options=_b('H\003'),
  serialized_pb=_b('\n\rGeoData.proto\x12\x04Rdmp\"U\n\x0eGeoCoordinates\x12\x10\n\x08latitude\x18\x01 \x02(\x01\x12\x11\n\tlongitude\x18\x02 \x02(\x01\x12\x10\n\x08\x61ltitude\x18\x03 \x02(\x01\x12\x0c\n\x04\x65psg\x18\x04 \x01(\r\"@\n\x0fRectCoordinates\x12\t\n\x01x\x18\x01 \x02(\x01\x12\t\n\x01y\x18\x02 \x02(\x01\x12\t\n\x01z\x18\x03 \x02(\x01\x12\x0c\n\x04\x65psg\x18\x04 \x01(\r\"7\n\x0bOrientation\x12\x0c\n\x04roll\x18\x01 \x02(\x02\x12\r\n\x05pitch\x18\x02 \x02(\x02\x12\x0b\n\x03yaw\x18\x03 \x02(\x02\"O\n\x05Point\x12!\n\x03geo\x18\x01 \x01(\x0b\x32\x14.Rdmp.GeoCoordinates\x12#\n\x04rect\x18\x02 \x01(\x0b\x32\x15.Rdmp.RectCoordinates\"0\n\nFloatRange\x12\x10\n\x08rangemin\x18\x01 \x02(\x02\x12\x10\n\x08rangemax\x18\x02 \x02(\x02\"o\n\x13\x41ltitudeRestriction\x12%\n\x0bheightRange\x18\x02 \x01(\x0b\x32\x10.Rdmp.FloatRange\x12\x31\n\theightRef\x18\x03 \x01(\x0e\x32\x15.Rdmp.HeightReference:\x07ZeroRef*<\n\x0fHeightReference\x12\x0b\n\x07ZeroRef\x10\x01\x12\x0e\n\nStartAsRef\x10\x02\x12\x0c\n\x08RoiAsRef\x10\x03\x42\x02H\x03')
)

_HEIGHTREFERENCE = _descriptor.EnumDescriptor(
  name='HeightReference',
  full_name='Rdmp.HeightReference',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='ZeroRef', index=0, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='StartAsRef', index=1, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='RoiAsRef', index=2, number=3,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=477,
  serialized_end=537,
)
_sym_db.RegisterEnumDescriptor(_HEIGHTREFERENCE)

HeightReference = enum_type_wrapper.EnumTypeWrapper(_HEIGHTREFERENCE)
ZeroRef = 1
StartAsRef = 2
RoiAsRef = 3



_GEOCOORDINATES = _descriptor.Descriptor(
  name='GeoCoordinates',
  full_name='Rdmp.GeoCoordinates',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='latitude', full_name='Rdmp.GeoCoordinates.latitude', index=0,
      number=1, type=1, cpp_type=5, label=2,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='longitude', full_name='Rdmp.GeoCoordinates.longitude', index=1,
      number=2, type=1, cpp_type=5, label=2,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='altitude', full_name='Rdmp.GeoCoordinates.altitude', index=2,
      number=3, type=1, cpp_type=5, label=2,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='epsg', full_name='Rdmp.GeoCoordinates.epsg', index=3,
      number=4, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=23,
  serialized_end=108,
)


_RECTCOORDINATES = _descriptor.Descriptor(
  name='RectCoordinates',
  full_name='Rdmp.RectCoordinates',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='x', full_name='Rdmp.RectCoordinates.x', index=0,
      number=1, type=1, cpp_type=5, label=2,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='y', full_name='Rdmp.RectCoordinates.y', index=1,
      number=2, type=1, cpp_type=5, label=2,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='z', full_name='Rdmp.RectCoordinates.z', index=2,
      number=3, type=1, cpp_type=5, label=2,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='epsg', full_name='Rdmp.RectCoordinates.epsg', index=3,
      number=4, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=110,
  serialized_end=174,
)


_ORIENTATION = _descriptor.Descriptor(
  name='Orientation',
  full_name='Rdmp.Orientation',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='roll', full_name='Rdmp.Orientation.roll', index=0,
      number=1, type=2, cpp_type=6, label=2,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='pitch', full_name='Rdmp.Orientation.pitch', index=1,
      number=2, type=2, cpp_type=6, label=2,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='yaw', full_name='Rdmp.Orientation.yaw', index=2,
      number=3, type=2, cpp_type=6, label=2,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=176,
  serialized_end=231,
)


_POINT = _descriptor.Descriptor(
  name='Point',
  full_name='Rdmp.Point',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='geo', full_name='Rdmp.Point.geo', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='rect', full_name='Rdmp.Point.rect', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=233,
  serialized_end=312,
)


_FLOATRANGE = _descriptor.Descriptor(
  name='FloatRange',
  full_name='Rdmp.FloatRange',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='rangemin', full_name='Rdmp.FloatRange.rangemin', index=0,
      number=1, type=2, cpp_type=6, label=2,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='rangemax', full_name='Rdmp.FloatRange.rangemax', index=1,
      number=2, type=2, cpp_type=6, label=2,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=314,
  serialized_end=362,
)


_ALTITUDERESTRICTION = _descriptor.Descriptor(
  name='AltitudeRestriction',
  full_name='Rdmp.AltitudeRestriction',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='heightRange', full_name='Rdmp.AltitudeRestriction.heightRange', index=0,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='heightRef', full_name='Rdmp.AltitudeRestriction.heightRef', index=1,
      number=3, type=14, cpp_type=8, label=1,
      has_default_value=True, default_value=1,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=364,
  serialized_end=475,
)

_POINT.fields_by_name['geo'].message_type = _GEOCOORDINATES
_POINT.fields_by_name['rect'].message_type = _RECTCOORDINATES
_ALTITUDERESTRICTION.fields_by_name['heightRange'].message_type = _FLOATRANGE
_ALTITUDERESTRICTION.fields_by_name['heightRef'].enum_type = _HEIGHTREFERENCE
DESCRIPTOR.message_types_by_name['GeoCoordinates'] = _GEOCOORDINATES
DESCRIPTOR.message_types_by_name['RectCoordinates'] = _RECTCOORDINATES
DESCRIPTOR.message_types_by_name['Orientation'] = _ORIENTATION
DESCRIPTOR.message_types_by_name['Point'] = _POINT
DESCRIPTOR.message_types_by_name['FloatRange'] = _FLOATRANGE
DESCRIPTOR.message_types_by_name['AltitudeRestriction'] = _ALTITUDERESTRICTION
DESCRIPTOR.enum_types_by_name['HeightReference'] = _HEIGHTREFERENCE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

GeoCoordinates = _reflection.GeneratedProtocolMessageType('GeoCoordinates', (_message.Message,), dict(
  DESCRIPTOR = _GEOCOORDINATES,
  __module__ = 'GeoData_pb2'
  # @@protoc_insertion_point(class_scope:Rdmp.GeoCoordinates)
  ))
_sym_db.RegisterMessage(GeoCoordinates)

RectCoordinates = _reflection.GeneratedProtocolMessageType('RectCoordinates', (_message.Message,), dict(
  DESCRIPTOR = _RECTCOORDINATES,
  __module__ = 'GeoData_pb2'
  # @@protoc_insertion_point(class_scope:Rdmp.RectCoordinates)
  ))
_sym_db.RegisterMessage(RectCoordinates)

Orientation = _reflection.GeneratedProtocolMessageType('Orientation', (_message.Message,), dict(
  DESCRIPTOR = _ORIENTATION,
  __module__ = 'GeoData_pb2'
  # @@protoc_insertion_point(class_scope:Rdmp.Orientation)
  ))
_sym_db.RegisterMessage(Orientation)

Point = _reflection.GeneratedProtocolMessageType('Point', (_message.Message,), dict(
  DESCRIPTOR = _POINT,
  __module__ = 'GeoData_pb2'
  # @@protoc_insertion_point(class_scope:Rdmp.Point)
  ))
_sym_db.RegisterMessage(Point)

FloatRange = _reflection.GeneratedProtocolMessageType('FloatRange', (_message.Message,), dict(
  DESCRIPTOR = _FLOATRANGE,
  __module__ = 'GeoData_pb2'
  # @@protoc_insertion_point(class_scope:Rdmp.FloatRange)
  ))
_sym_db.RegisterMessage(FloatRange)

AltitudeRestriction = _reflection.GeneratedProtocolMessageType('AltitudeRestriction', (_message.Message,), dict(
  DESCRIPTOR = _ALTITUDERESTRICTION,
  __module__ = 'GeoData_pb2'
  # @@protoc_insertion_point(class_scope:Rdmp.AltitudeRestriction)
  ))
_sym_db.RegisterMessage(AltitudeRestriction)


DESCRIPTOR._options = None
# @@protoc_insertion_point(module_scope)
