// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: InfoPacket.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace Rdmp {

  /// <summary>Holder for reflection information generated from InfoPacket.proto</summary>
  public static partial class InfoPacketReflection {

    #region Descriptor
    /// <summary>File descriptor for InfoPacket.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static InfoPacketReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "ChBJbmZvUGFja2V0LnByb3RvEgRSZG1wIt8BCgpJbmZvUGFja2V0EgwKBHR5",
            "cGUYASACKA0SCgoCaWQYAiACKA0SDAoEZGF0YRgDIAEoDCKoAQoKUGFja2V0",
            "VHlwZRIQCgxOb3RpZmljYXRpb24QARIaChZPcGVyYXRpb25zSW5zdHJ1Y3Rp",
            "b25zEAkSIAocT3BlcmF0aW9uc0luc3RydWN0aW9uc1JlcG9ydBAMEhAKDEZp",
            "bGVGcmFnbWVudBAOEg8KC0ZpbGVSZXF1ZXN0EA8SEwoMVmVyZXNrUmVzdWx0",
            "EIHggAESEgoLVmVyZXNrRXZlbnQQguCAAUICSAM="));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { },
          new pbr::GeneratedClrTypeInfo(null, null, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::Rdmp.InfoPacket), global::Rdmp.InfoPacket.Parser, new[]{ "Type", "Id", "Data" }, null, new[]{ typeof(global::Rdmp.InfoPacket.Types.PacketType) }, null, null)
          }));
    }
    #endregion

  }
  #region Messages
  /// <summary>
  //// Информационный пакет
  /// </summary>
  public sealed partial class InfoPacket : pb::IMessage<InfoPacket>
  #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      , pb::IBufferMessage
  #endif
  {
    private static readonly pb::MessageParser<InfoPacket> _parser = new pb::MessageParser<InfoPacket>(() => new InfoPacket());
    private pb::UnknownFieldSet _unknownFields;
    private int _hasBits0;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pb::MessageParser<InfoPacket> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Rdmp.InfoPacketReflection.Descriptor.MessageTypes[0]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public InfoPacket() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public InfoPacket(InfoPacket other) : this() {
      _hasBits0 = other._hasBits0;
      type_ = other.type_;
      id_ = other.id_;
      data_ = other.data_;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public InfoPacket Clone() {
      return new InfoPacket(this);
    }

    /// <summary>Field number for the "type" field.</summary>
    public const int TypeFieldNumber = 1;
    private readonly static uint TypeDefaultValue = 0;

    private uint type_;
    /// <summary>
    //// Тип пакета
    /// </summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public uint Type {
      get { if ((_hasBits0 & 1) != 0) { return type_; } else { return TypeDefaultValue; } }
      set {
        _hasBits0 |= 1;
        type_ = value;
      }
    }
    /// <summary>Gets whether the "type" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasType {
      get { return (_hasBits0 & 1) != 0; }
    }
    /// <summary>Clears the value of the "type" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearType() {
      _hasBits0 &= ~1;
    }

    /// <summary>Field number for the "id" field.</summary>
    public const int IdFieldNumber = 2;
    private readonly static uint IdDefaultValue = 0;

    private uint id_;
    /// <summary>
    //// Идентификатор пакета
    /// </summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public uint Id {
      get { if ((_hasBits0 & 2) != 0) { return id_; } else { return IdDefaultValue; } }
      set {
        _hasBits0 |= 2;
        id_ = value;
      }
    }
    /// <summary>Gets whether the "id" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasId {
      get { return (_hasBits0 & 2) != 0; }
    }
    /// <summary>Clears the value of the "id" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearId() {
      _hasBits0 &= ~2;
    }

    /// <summary>Field number for the "data" field.</summary>
    public const int DataFieldNumber = 3;
    private readonly static pb::ByteString DataDefaultValue = pb::ByteString.Empty;

    private pb::ByteString data_;
    /// <summary>
    //// Данные пакета
    /// </summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public pb::ByteString Data {
      get { return data_ ?? DataDefaultValue; }
      set {
        data_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "data" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasData {
      get { return data_ != null; }
    }
    /// <summary>Clears the value of the "data" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearData() {
      data_ = null;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override bool Equals(object other) {
      return Equals(other as InfoPacket);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool Equals(InfoPacket other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (Type != other.Type) return false;
      if (Id != other.Id) return false;
      if (Data != other.Data) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override int GetHashCode() {
      int hash = 1;
      if (HasType) hash ^= Type.GetHashCode();
      if (HasId) hash ^= Id.GetHashCode();
      if (HasData) hash ^= Data.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void WriteTo(pb::CodedOutputStream output) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      output.WriteRawMessage(this);
    #else
      if (HasType) {
        output.WriteRawTag(8);
        output.WriteUInt32(Type);
      }
      if (HasId) {
        output.WriteRawTag(16);
        output.WriteUInt32(Id);
      }
      if (HasData) {
        output.WriteRawTag(26);
        output.WriteBytes(Data);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalWriteTo(ref pb::WriteContext output) {
      if (HasType) {
        output.WriteRawTag(8);
        output.WriteUInt32(Type);
      }
      if (HasId) {
        output.WriteRawTag(16);
        output.WriteUInt32(Id);
      }
      if (HasData) {
        output.WriteRawTag(26);
        output.WriteBytes(Data);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(ref output);
      }
    }
    #endif

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public int CalculateSize() {
      int size = 0;
      if (HasType) {
        size += 1 + pb::CodedOutputStream.ComputeUInt32Size(Type);
      }
      if (HasId) {
        size += 1 + pb::CodedOutputStream.ComputeUInt32Size(Id);
      }
      if (HasData) {
        size += 1 + pb::CodedOutputStream.ComputeBytesSize(Data);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(InfoPacket other) {
      if (other == null) {
        return;
      }
      if (other.HasType) {
        Type = other.Type;
      }
      if (other.HasId) {
        Id = other.Id;
      }
      if (other.HasData) {
        Data = other.Data;
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(pb::CodedInputStream input) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      input.ReadRawMessage(this);
    #else
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 8: {
            Type = input.ReadUInt32();
            break;
          }
          case 16: {
            Id = input.ReadUInt32();
            break;
          }
          case 26: {
            Data = input.ReadBytes();
            break;
          }
        }
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalMergeFrom(ref pb::ParseContext input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, ref input);
            break;
          case 8: {
            Type = input.ReadUInt32();
            break;
          }
          case 16: {
            Id = input.ReadUInt32();
            break;
          }
          case 26: {
            Data = input.ReadBytes();
            break;
          }
        }
      }
    }
    #endif

    #region Nested types
    /// <summary>Container for nested types declared in the InfoPacket message type.</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static partial class Types {
      /// <summary>
      //// Возможные типы пакета
      /// </summary>
      public enum PacketType {
        /// <summary>
        //// Уведомление о получении пакета (в обе стороны)
        /// </summary>
        [pbr::OriginalName("Notification")] Notification = 1,
        /// <summary>
        //// Распоряжения (к НСУ)
        /// </summary>
        [pbr::OriginalName("OperationsInstructions")] OperationsInstructions = 9,
        /// <summary>
        //// Доклад о готовности к выполнению распоряжений
        /// </summary>
        [pbr::OriginalName("OperationsInstructionsReport")] OperationsInstructionsReport = 12,
        /// <summary>
        //// Фрагмент файла
        /// </summary>
        [pbr::OriginalName("FileFragment")] FileFragment = 14,
        /// <summary>
        //// Запрос файла
        /// </summary>
        [pbr::OriginalName("FileRequest")] FileRequest = 15,
        /// <summary>
        //// Результат выполнения задания от комплекса "Вереск"
        /// </summary>
        [pbr::OriginalName("VereskResult")] VereskResult = 2109441,
        /// <summary>
        //// Уведомление от комплекса "Вереск"
        /// </summary>
        [pbr::OriginalName("VereskEvent")] VereskEvent = 2109442,
      }

    }
    #endregion

  }

  #endregion

}

#endregion Designer generated code
