﻿using Rdmp;
using System;

namespace VereskClientDll
{
    public struct VereskMessage
    {
        public MessageHeader Header;
        public InfoPacket InfoPacket;
        public byte[] Payload;

        public long MessageSize { get => MessageHeader.BinarySize + Header.InfoPacketLength + Header.PayloadLength; }

        public VereskMessage(MessageHeader header, InfoPacket info, byte[] payload)
        {
            Header = header;
            InfoPacket = info;
            Payload = payload;
        }

        public VereskMessage(byte[] data)
        {
            var headerBuffer = new byte[MessageHeader.BinarySize];
            //var header = new MessageHeader();

            Array.Copy(data, headerBuffer, MessageHeader.BinarySize);
            MessageHeader.TryParse(headerBuffer, out Header);

            var bufferInfoPacket = new byte[Header.InfoPacketLength];
            InfoPacket = null;

            if (Header.InfoPacketLength != 0)
            {
                Array.Copy(data, MessageHeader.BinarySize, bufferInfoPacket, 0, Header.InfoPacketLength);
                InfoPacket = InfoPacket.Parser.ParseFrom(bufferInfoPacket);
            }

            Payload = new byte[Header.PayloadLength];

            if (Header.PayloadLength != 0)
            {
                Array.Copy(data, MessageHeader.BinarySize + Header.InfoPacketLength, Payload, 0, Header.PayloadLength);
            }
        }
    }
}
