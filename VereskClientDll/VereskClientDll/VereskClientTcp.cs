﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Google.Protobuf;
using Rdmp;
using Rdmp.Veresk;
using System.Diagnostics;
using SimpleTcp;
using System.Collections.ObjectModel;

namespace VereskClientDll
{
    public class VereskClientTcp
    {
        //private ClientTcp _clientTcp;
        //public string ProgramName { get; set; } = "Kvetka";
        //public PrivLevel PrivilegeLevel  { get; private set; } = PrivLevel.PlAdmin;
        //public string Token { get; private set; } = "";
        public event EventHandler OnConnect;
        public event EventHandler OnDisconnect;
        public event EventHandler<byte[]> OnReadBytes;
        public event EventHandler<byte[]> OnWriteBytes;

        private SimpleTcpClient Client; //= new SimpleTcpClient("127.0.0.1", 32133)

        private Uuid LastUuid;
        // Здесь лежит словарь, ключ - uuid файла, значение - его имя
        Dictionary<string, string> dicUuid = new Dictionary<string, string>();


        #region SimpleTCP

        /// <summary>
        /// Gets of sets time to wait for a response to a request.
        /// </summary>
        public int TimeoutResponce { get; set; } = 10000;

        //public Action<string> Logger { get => Client.Logger; set => Client.Logger = value; } 

        /// <summary>
        /// Подключиться к серверу
        /// </summary>
        /// <param name="ipAddress">IP адрес сервера</param>
        /// <param name="port">Порт сервера</param>
        public void Connect(string ipAddress, int port)
        {
            if (Client != null)
                Disconnect();

            try
            {
                Client = new SimpleTcpClient(ipAddress, port);
                Client.Events.DataReceived += Events_DataReceived;

                Client.Connect();
                OnConnect?.Invoke(this, null);
            }
            catch (Exception e)
            { }
        }

        private void Events_DataReceived(object sender, SimpleTcp.DataReceivedEventArgs e)
        {
            OnReadBytes?.Invoke(this, e.Data);
            DecodePackage(e.Data);
        }




        /// <summary>
        /// Отключиться от сервера
        /// </summary>
        public void Disconnect()
        {
            try
            {
                if (Client == null)
                    return;

                Client.Disconnect();
                Client.Events.DataReceived -= Events_DataReceived;
                Client.Dispose();
                Client = null;

                OnDisconnect?.Invoke(this, null);
            }
            catch (Exception e)
            {
            }
        }



        private async Task<IMessage> SendRequestAndGetReply(InfoPacket message) //мб заменить infopacket на IMassage
        {
            var messageBytes = message.ToByteArray();
            MessageHeader header = new MessageHeader(messageBytes.Length, 0, 0);

            var bSend = header.GetBytes().Concat(messageBytes).ToArray();

            try
            {
                var response = WriteBytesAndGetReply(bSend, TimeSpan.FromMilliseconds(TimeoutResponce));

                if (response != null)
                {
                    var responseMessage = await DecodePackageAfterRequest(response);
                    return responseMessage;
                }
                return null;
            }
            catch (TimeoutException)
            {
                return null;
            }
            catch (NullReferenceException)
            {
                return null;
            }
        }

        private byte[] WriteBytesAndGetReply(byte[] data, TimeSpan timeout)
        {
            byte[] mReply = null;
            Client.Events.DataReceived += (s, e) => { mReply = e.Data; };
            Client?.SendAsync(data);
            OnWriteBytes?.Invoke(this, data);

            Stopwatch sw = new Stopwatch();
            sw.Start();

            while (mReply == null && sw.Elapsed < timeout)
            {
                System.Threading.Thread.Sleep(10);
            }

            return mReply;
        }


        private async System.Threading.Tasks.Task SendRequest(InfoPacket message)
        {
            var messageBytes = message.ToByteArray();
            MessageHeader header = new MessageHeader(messageBytes.Length, 0, 0);

            var bSend = header.GetBytes().Concat(messageBytes).ToArray();

            try
            {
                Client?.SendAsync(bSend);
                OnWriteBytes?.Invoke(this, bSend);
            }
            catch (Exception e)
            { }
        }



        private async Task<IMessage> DecodePackageAfterRequest(byte[] data)
        {
            var package = new VereskMessage(data);
            if (data.Length > package.MessageSize)
            {
                byte[] temp = new byte[data.Length - package.MessageSize];
                Array.Copy(data, package.MessageSize, temp, 0, temp.Length);
                package = new VereskMessage(temp);
            }

            var message = ParseDataFieldNoEvent(package.InfoPacket, package.Payload);
            return message;
        }

        private void DecodePackage(byte[] data)
        {
            var package = new VereskMessage(data);
            ParseDataField(package.InfoPacket, package.Payload);

            int i = (int)package.MessageSize;
            while (i < data.Length)
            {
                byte[] temp = new byte[data.Length - i];
                Array.Copy(data, i, temp, 0, temp.Length);
                package = new VereskMessage(temp);
                i += (int)package.MessageSize;
                ParseDataField(package.InfoPacket, package.Payload);
            }
        }



        public event EventHandler<VereskEvent> OnGetVereskEvent;
        public event EventHandler<VereskResult> OnGetVereskResult;
        public event EventHandler OnGetVereskNotification;
        public event EventHandler<FileFragment> OnGetVereskFileFragment;
        public event EventHandler<OperationsInstructionsReport> OnGetOperationsInstructionsReport;
        private IMessage ParseDataFieldNoEvent(InfoPacket infoPacket, byte[] payload)
        {
            IMessage mess = null;
            switch ((InfoPacket.Types.PacketType)infoPacket.Type)
            {
                case InfoPacket.Types.PacketType.VereskEvent:
                    var ev = VereskEvent.Parser.ParseFrom(infoPacket.Data);
                    mess = ev;
                    break;
                case InfoPacket.Types.PacketType.VereskResult:
                    var res = VereskResult.Parser.ParseFrom(infoPacket.Data);
                    mess = res;
                    break;
                case InfoPacket.Types.PacketType.Notification:
                    mess = infoPacket;
                    break;
                case InfoPacket.Types.PacketType.FileFragment:
                    var fragment = FileFragment.Parser.ParseFrom(infoPacket.Data);
                    WriteReceivedFile(fragment, payload);
                    mess = fragment;
                    break;
                case InfoPacket.Types.PacketType.OperationsInstructionsReport:
                    var oiReport = OperationsInstructionsReport.Parser.ParseFrom(infoPacket.Data);
                    mess = oiReport;
                    break;
                default:
                    break;
            }

            return mess;
        }
        private void ParseDataField(InfoPacket infoPacket, byte[] payload)
        {
            switch ((InfoPacket.Types.PacketType)infoPacket.Type)
            {
                case InfoPacket.Types.PacketType.VereskEvent:
                    var ev = VereskEvent.Parser.ParseFrom(infoPacket.Data);
                    OnGetVereskEvent?.Invoke(this, ev);
                    break;
                case InfoPacket.Types.PacketType.VereskResult:
                    var res = VereskResult.Parser.ParseFrom(infoPacket.Data);
                    OnGetVereskResult?.Invoke(this, res);
                    break;
                case InfoPacket.Types.PacketType.Notification:
                    OnGetVereskNotification?.Invoke(this, null);
                    break;
                case InfoPacket.Types.PacketType.FileFragment:
                    var fragment = FileFragment.Parser.ParseFrom(infoPacket.Data);
                    WriteReceivedFile(fragment, payload);
                    OnGetVereskFileFragment?.Invoke(this, null);
                    System.Threading.Tasks.Task.Run(() => SendRequest(new InfoPacket() { Id = infoPacket.Id, Type = (uint)InfoPacket.Types.PacketType.Notification }));
                    break;
                case InfoPacket.Types.PacketType.OperationsInstructionsReport:
                    var oiReport = OperationsInstructionsReport.Parser.ParseFrom(infoPacket.Data);
                    OnGetOperationsInstructionsReport?.Invoke(this, oiReport);
                    break;
                default:
                    break;
            }
        }

        private void WriteReceivedFile(FileFragment fragment, byte[] payload)
        {
            try
            {
                // Записываем uuid группы заданий в файл, чтобы потом можно было
                // прочитать его, когда нам понадобится её удалить
                using (FileStream fstream = new FileStream($".\\records\\" + UuidToString(fragment.Uuid) + "_" + fragment.Filename, FileMode.OpenOrCreate))
                {
                    fstream.Write(payload);
                }
            }
            catch (Exception ex) { }
        }

        #endregion




        #region Commands

        private static uint _counter = 0;
        /// <summary>
        ///  Возвращает новый идентификатор пакета
        /// :return: Целое число - идентификатор пакета
        /// </summary>
        private uint GetNextPacketId()
        {
            _counter += 1;
            return _counter - 1;
        }

        /// <summary>
        /// Постановка задания от клиента с базовыми настройками
        /// </summary>
        /// <param name="sender">Имя поста отправителя</param>
        /// <param name="dest">Имя поста, выполняющего задание</param>
        /// <returns>Ответ сервера на постановку задания</returns>
        public async Task<OperationsInstructionsReport> AddTask()
        {
            try
            {
                List<Rdmp.Veresk.Task> tasks = new List<Rdmp.Veresk.Task>();
                tasks.Add(CreateVereskTask("Задание от " + DateTime.Now.ToString("dd-mm-yy HH:mm:ssS"), false));
                (InfoPacket, Uuid) information = CreateInfoPacketWithTasks(tasks);
                LastUuid = information.Item2;

                try
                {
                    // Записываем uuid группы заданий в файл, чтобы потом можно было
                    // прочитать его, когда нам понадобится её удалить
                    using (FileStream fstream = new FileStream($".\\task_uuid.txt", FileMode.OpenOrCreate))
                    {
                        fstream.Write(information.Item2.ToByteArray()); //вообще надо по-другому записывать, но пока пох
                                                                        //// запись массива байтов в файл
                                                                        //fstream.Write(bufferPayload, 0, bufferPayload.);
                        Console.WriteLine("Текст записан в файл");
                    }
                }
                catch (Exception ex) { }


                var reply = SendRequestAndGetReply(information.Item1).Result as OperationsInstructionsReport;
                return reply;
            }
            catch (Exception e)
            {
                //return false;
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Постановка задания от клиента
        /// </summary>
        /// <param name="listSignalClasses">Классы сигналов, которые хотим обнаруживать</param>
        /// <param name="listFreqRanges">Диапазоны рабочих частот</param>
        /// <param name="listSpecFreqs">Отдельные частоты для мониторинга</param>
        /// <param name="taskType">Тип задания: поиск в диапазонах/мониторинг отдельных частот</param>
        /// <param name="requestedChannels">Количество узкополосных каналов на задание</param>
        /// <returns>Ответ сервера на постановку задания</returns>
        public async Task<OperationsInstructionsReport> AddTask(List<SignalClass> listSignalClasses, List<FreqRange> listFreqRanges, List<uint> listSpecFreqs, Rdmp.Veresk.Task.Types.TaskType taskType = Rdmp.Veresk.Task.Types.TaskType.TtSearch,
             uint requestedChannels = 500)
        {
            try
            {
                List<Rdmp.Veresk.Task> tasks = new List<Rdmp.Veresk.Task>();
                tasks.Add(CreateVereskTask("Задание от " + DateTime.Now.ToString("dd-mm-yy HH:mm:ssS"), listSignalClasses, listFreqRanges, listSpecFreqs, taskType, requestedChannels));
                (InfoPacket, Uuid) information = CreateInfoPacketWithTasks(tasks);
                LastUuid = information.Item2;

                try
                {
                    // Записываем uuid группы заданий в файл, чтобы потом можно было
                    // прочитать его, когда нам понадобится её удалить
                    using (FileStream fstream = new FileStream($".\\task_uuid.txt", FileMode.OpenOrCreate))
                    {
                        fstream.Write(information.Item2.ToByteArray()); //вообще надо по-другому записывать, но пока пох
                                                                        //// запись массива байтов в файл
                                                                        //fstream.Write(bufferPayload, 0, bufferPayload.);
                        Console.WriteLine("Текст записан в файл");
                    }
                }
                catch (Exception ex) { }


                var reply = SendRequestAndGetReply(information.Item1).Result as OperationsInstructionsReport;

                return reply;
            }
            catch (Exception e)
            {
                //return false;
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Удалить последнее задание
        /// </summary>
        /// <returns>Ответ сервера на удаление задания</returns>
        public async Task<OperationsInstructionsReport> DeleteTask()
        {
            try
            {
                var del_task_packet = CreateDeleteTaskRequest(LastUuid);


                var answer = SendRequestAndGetReply(del_task_packet).Result as OperationsInstructionsReport;
                return answer;
            }
            catch (Exception e)
            {
                //return false;
                throw new Exception(e.Message);
            }
        }

        ///Создает объект типа FileRequest
        ///   :param file_uuid: объект - идентификатор файла
        ///   : return: объект типа FileRequest, упакованный внутрь объекта InfoPacket
        private InfoPacket CreateFileRequest(Uuid file_uuid) {
            //print("Creating file request with uuid {}...".format(uuid2string(file_uuid)))

            var fr = new FileRequest();
            fr.Uuid = file_uuid;
            fr.RequestType = FileRequest.Types.FileRequestStatus.Request;
            fr.MaxFragmentSize = 524288;

            var ip = new InfoPacket();
            ip.Type = (uint)InfoPacket.Types.PacketType.FileRequest;
            ip.Id = GetNextPacketId();
            ip.Data = fr.ToByteString();

            return ip;
        }
        ///Разбирает список обработанных файлов и отсылает file request для каждого из них
        ///   :param s: сокет
        ///   : param pf: список обработанных файлов(объектов типа ProcessedFileMetadata)
        ///   :return: None
        private void ParseProcessedFiles(List<ProcessedFileMetadata> listFiles) {

            foreach (var file in listFiles)
            {
                var s_uuid = UuidToString(file.FileUuid);
                // Заполним словарь, чтобы потом понять, как uuid файла соотносится с его именем
                dicUuid.Add(s_uuid, file.DataFileName);
                var file_request_packet = CreateFileRequest(file.FileUuid);
                var answer = SendRequestAndGetReply(file_request_packet);
            }
        }


        private Uuid GetUuid(int id = -1)
        {
            //var u = new Uuid();
            var temp = System.Guid.NewGuid().ToByteArray();

            byte[] ub;
            if (id != -1)
            {
                ub = temp.Take(12).ToArray().Concat(BitConverter.GetBytes(id)).ToArray(); //проверить порядок байт
            }
            else
            {
                ub = temp;
            }

            var p1 = BitConverter.ToUInt64(ub.Take(8).ToArray());
            var p2 = BitConverter.ToUInt64(ub.TakeLast(8).ToArray());
            var uuid_obj = new Uuid();
            uuid_obj.P1 = p1;
            uuid_obj.P2 = p2;
            return uuid_obj;
        }

        private (InfoPacket, Uuid) CreateInfoPacketWithTasks(List<Rdmp.Veresk.Task> task_vec)
        {
            var oi = new OperationsInstructions();
            oi.Id = GetUuid();
            oi.Type = (uint)OperationsInstructions.Types.Type.CreateOrUpdate;
            oi.Title = "Группа заданий №1";
            //oi.Duration
            foreach (var vt in task_vec)
            {
                var roi = new Roi();
                roi.Id = GetUuid();

                var roi_ins = new RoiInstructions();
                roi_ins.Roi = roi;


                var ot = new OperationTask();
                ot.SpecificTaskType = (uint)OperationTask.Types.SpecificTaskType.Veresk;
                ot.SpecificTask = vt.ToByteString();
                roi_ins.OperationTasks.Add(ot);

                oi.Instructions.Add(roi_ins);
            }

            var ip = new InfoPacket();
            ip.Type = (uint)InfoPacket.Types.PacketType.OperationsInstructions;
            ip.Id = GetNextPacketId();
            ip.Data = oi.ToByteString();

            return (ip, oi.Id);
        }


        /// <summary>
        ///Создает новое задание для Вереска для поиска сигналов с фазовой манипуляцией, а также ALE - 2G
        ///: param name: str - имя нового задания
        ///:param is_all_dets: bool -флаг, требующий включить в задание все обнаружители
        ///:return Veresk_common_pb2.Task: Объект типа Veresk_common_pb2.Task
        /// </summary>
        private Rdmp.Veresk.Task CreateVereskTask(string name, bool is_all_dets)
        {


            // Для начала создаем объект задания и заполняем поля его имени и типа (поиск/наблюдение)
            var vt = new Rdmp.Veresk.Task();
            vt.TaskName = name;
            vt.TaskType = Rdmp.Veresk.Task.Types.TaskType.TtSearch;
            // Задание на мониторинг (отдельных частот) нужно создавать вот так:
            //vt.task_type = Veresk_common_pb2.Task.TT_MONITORING

            // Теперь заполним классы сигналов, которые нужно обнаруживать в задании
            vt.SigClasses.AddRange(new List<SignalClass>{SignalClass.ScAle2G,
                                   SignalClass.ScAle3G,
                                   SignalClass.ScSt4529,
                                   SignalClass.ScMil110A,
                                   SignalClass.ScMil110BAppc,
                                   SignalClass.ScMil110CAppd,
                                   SignalClass.ScDc7200,
                                   SignalClass.ScLink22,
                                   SignalClass.ScB162 });

            // Включим сомнительный с точки зрения необходимости обнаружитель ST4285, если включен флаг.
            if (is_all_dets)
            { vt.SigClasses.Add(SignalClass.ScSt4285); }

            // Не надо нам посылать запросы на пеленгатор, он и так через БД увидит нашу работу
            // и подпишет пеленги
            vt.RequestDirections = false;
            vt.RequestLocations = false;

            // Использовать оперативный откат - хорошая идея для заданий на поиск
            vt.UseFastRollback = true;

            // Добавим диапазон частот 1.5 ... 30 МГц в задание с шагом в 2.5 кГц
            var default_freq_range = new FreqRange();
            default_freq_range.StartFreq = 1500000;
            default_freq_range.EndFreq = 30000000;
            default_freq_range.Delta = 2500;

            vt.FreqRanges.Add(default_freq_range);
            // Можно вот так добавить отдельные частоты (например, в задание для мониторинга):
            // vt.freqs.append(9795000)
            // vt.freqs.append(7845000)

            // Запросим, например, 500 каналов для этого задания
            vt.RequestedChannels = 500;

            return vt;
        }


        /// <summary>
        ///Создает новое задание для Вереска для поиска сигналов с фазовой манипуляцией, а также ALE - 2G
        ///: param name: str - имя нового задания
        ///:param is_all_dets: bool -флаг, требующий включить в задание все обнаружители
        ///:return Veresk_common_pb2.Task: Объект типа Veresk_common_pb2.Task
        /// </summary>
        private Rdmp.Veresk.Task CreateVereskTask(string name, List<SignalClass> listSignalClasses, List<FreqRange> listFreqRanges, List<uint> listSpecFreqs, Rdmp.Veresk.Task.Types.TaskType taskType = Rdmp.Veresk.Task.Types.TaskType.TtSearch, 
             uint requestedChannels = 500)
        {
            // Для начала создаем объект задания и заполняем поля его имени и типа (поиск/наблюдение)
            var vt = new Rdmp.Veresk.Task();
            vt.TaskName = name;
            vt.TaskType = taskType;

            // Теперь заполним классы сигналов, которые нужно обнаруживать в задании
            vt.SigClasses.AddRange(new List<SignalClass>(listSignalClasses));

            // Не надо нам посылать запросы на пеленгатор, он и так через БД увидит нашу работу
            // и подпишет пеленги
            vt.RequestDirections = false;
            vt.RequestLocations = false;

            // Использовать оперативный откат - хорошая идея для заданий на поиск
            vt.UseFastRollback = true;

            // Добавим диапазон частот
            vt.FreqRanges.AddRange(listFreqRanges);
            // Можно вот так добавить отдельные частоты (например, в задание для мониторинга):
            vt.Freqs.AddRange(listSpecFreqs);

            // Запросим, например, 500 каналов для этого задания
            vt.RequestedChannels = requestedChannels;

            return vt;
        }

       


        /// <summary>
        ///Удаляет задание или группу заданий Вереска
        ///param/// object -уникальный идентификатор задания, которое нужно удалить
        ///:return object: Объект типа InfoPacket, содержащий внутри запрос на удаление задания
        /// </summary>
        private InfoPacket CreateDeleteTaskRequest(Uuid uuid)
        {
            var oi = new OperationsInstructions();
            oi.Id = uuid;
            oi.Type = (uint)OperationsInstructions.Types.Type.Cancel;

            var ip = new InfoPacket();
            ip.Type = (uint)InfoPacket.Types.PacketType.OperationsInstructions;
            ip.Id = GetNextPacketId();
            ip.Data = oi.ToByteString();

            return ip;
        }

        /// <summary>
        /// Конвертирует объект типа Rdmp.Uuid в строку
        ///:param uuid: объект типа Rdmp.Uuid
        ///:return: строка с шестнадцатиричным представлением uuid
        /// </summary>
        private string UuidToString(Uuid uuid)
        {
            var b1 = Convert.ToHexString(BitConverter.GetBytes(uuid.P1));
            var b2 = Convert.ToHexString(BitConverter.GetBytes(uuid.P2));

            return b1 + b2;
        }




        #endregion
    }
}
