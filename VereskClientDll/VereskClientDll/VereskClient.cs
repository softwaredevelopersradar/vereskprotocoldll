﻿//using Google.Protobuf;
//using Rdmp;
//using Rdmp.Veresk;
using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using WatsonTcp;

namespace VereskClientDll
{
    public class VereskClient
    {
//        private ClientTcp _clientTcp;
//        //public string ProgramName { get; set; } = "Kvetka";
//        //public PrivLevel PrivilegeLevel  { get; private set; } = PrivLevel.PlAdmin;
//        //public string Token { get; private set; } = "";
//        public event EventHandler OnConnect;
//        public event EventHandler OnDisconnect;
//        public event EventHandler<byte[]> OnReadBytes;
//        public event EventHandler<byte[]> OnWriteBytes;

//        private WatsonTcpClient Client;

//        private Uuid LastUuid;


//        /// <summary>
//        /// Method to invoke when sending a log message.
//        /// </summary>
//        public Action<Severity, string> Logger
//        {
//            get
//            {
//                return this.Client.Settings.Logger;
//            }
//            set
//            {
//                this.Client.Settings.Logger = value;
//            }
//        }

//        public VereskClient()
//        {
//        }

//        //public VereskClient(PrivLevel privilegeLevel, string login, string password)
//        //{
//        //    PrivilegeLevel = privilegeLevel;
//        //    Token = login + password;
//        //}

//        #region Watson

//        /// <summary>
//        /// Gets of sets time to wait for a response to a request.
//        /// </summary>
//        public int Timeout { get; set; } = 10000;

//        ///// <summary>
//        ///// Gets or sets a property used to customize the display of messages showed in <see cref="Logger"/>.
//        ///// </summary>
//        //public FormatType LoggerFormat { get; set; } = FormatType.Full;


//        /// <summary>
//        /// Initiate connection to TCP server.
//        /// </summary>
//        public void Connect(string ipAddress, int port)
//        {
//            if (Client != null)
//                Disconnect();

//            //bool result = false;

//            try
//            {
                
//                Client = new WatsonTcpClient(ipAddress, port);
//                Client.Keepalive.TcpKeepAliveRetryCount = 3;
//                Client.Keepalive.EnableTcpKeepAlives = false;
//                Client.Settings.AcceptInvalidCertificates = false;
//                Client.Settings.DebugMessages = false;
//                Client.Settings.MutuallyAuthenticate = false;
                
//                SubscribeToWatsonTcpEvents();
//                Client.Connect();
////#if NET5_0
////                if (Environment.OSVersion.Platform == PlatformID.Win32NT && Environment.OSVersion.Version.Major < 10)
////                {
////                    this.Client.Keepalive.EnableTcpKeepAlives = false;
////                    //doesn't work properly on win7.
////                }
////                else
////                {
////                    this.Client.Keepalive.EnableTcpKeepAlives = true;
////                }
////#elif NETFRAMEWORK
////                this.Client.Keepalive.EnableTcpKeepAlives = true;
////#elif NETSTANDARD
////                this.Client.Keepalive.EnableTcpKeepAlives = false;
////                this.Logger?.Invoke(Severity.Alert, "Tcp keep alives not available in netstandard.");
////#endif

//            }
//            catch (InvalidOperationException e)
//            {
//                this.Logger?.Invoke(Severity.Error, e.Message);
//            }
//            catch
//            {
//                // ignored
//            }
//        }

//        private void SubscribeToWatsonTcpEvents()
//        {
//            Client.Events.ServerConnected += (sender, args) => OnConnect?.Invoke(this, args);
//            Client.Events.ServerDisconnected += (sender, args) => OnDisconnect?.Invoke(this, args);
//            Client.Events.MessageReceived += Events_MessageReceived; //(sender, args) => OnReadBytes?.Invoke(this, args.Data);
//        }

//        private void Events_MessageReceived(object sender, MessageReceivedEventArgs e)
//        {
//            OnReadBytes?.Invoke(this, e.Data);
//            ReadMessage(e.Data);
//        }

//        public void Disconnect()
//        {
//            try
//            {
//                if (Client == null)
//                    return;

//                Client.Disconnect(); //TODO: проверить, отпишется ли от событий сам
//                Client.Dispose(); //TODO: мб не надо
//                Client = null;
//            }
//            catch (InvalidOperationException e)
//            {
//                this.Logger.Invoke(Severity.Alert, e.Message);
//            }
//            catch (NullReferenceException e)
//            {
//            }
//        }

//        private async Task<InfoPacket> SendRequest(InfoPacket message) //мб заменить infopacket на IMassage
//        {
//            var messageBytes = message.ToByteArray();
//            MessageHeader header = new MessageHeader(messageBytes.Length, 0, 0);

//            var bSend = header.GetBytes().Concat(messageBytes).ToArray();

//            try
//            {
//                SyncResponse response = Client?.SendAndWait(this.Timeout, bSend);
//                OnWriteBytes?.Invoke(this, bSend);
//                var responsePackage = await ReadMessage(response?.Data);
//                this.Logger?.Invoke(Severity.Info,
//                    $"Response received: {(InfoPacket.Types.PacketType)responsePackage.Type} \n");  //{responsePackage.ToString(LoggerFormat)}
//                return responsePackage;
//            }
//            catch (TimeoutException)
//            {
//                this.Logger?.Invoke(Severity.Alert,
//                    $"{(InfoPacket.Types.PacketType)message.Type}: no response during the specified time.\n");
//                return null;
//            }
//            catch (NullReferenceException)
//            {
//                this.Logger?.Invoke(Severity.Alert,
//                    $"{(InfoPacket.Types.PacketType)message.Type}: Network error.\n");
//                return null;
//            }
//        }

//        private async System.Threading.Tasks.Task SendRequestNoWaiting(InfoPacket message) //мб заменить infopacket на IMassage
//        {
//            var messageBytes = message.ToByteArray();
//            MessageHeader header = new MessageHeader(messageBytes.Length, 0, 0);

//            var bSend = header.GetBytes().Concat(messageBytes).ToArray();

//            try
//            {
//                Client?.Send(bSend, null, 28);
//                OnWriteBytes?.Invoke(this, bSend);
//            }
//            catch (NullReferenceException)
//            {
//                this.Logger?.Invoke(Severity.Alert,
//                    $"{(InfoPacket.Types.PacketType)message.Type}: Network error.\n");
//            }
//        }


//        private async Task<InfoPacket> ReadMessage(byte[] data)
//        {
//            var headerBuffer = new byte[MessageHeader.BinarySize];
//            var header = new MessageHeader();

//            Array.Copy(data, headerBuffer, MessageHeader.BinarySize);
//            MessageHeader.TryParse(headerBuffer, out header);


//            var bufferInfoPacket = new byte[header.InfoPacketLength];
//            InfoPacket infoPacket = null;

//            if (header.InfoPacketLength != 0)
//            {
//                Array.Copy(data, MessageHeader.BinarySize, bufferInfoPacket, 0, header.InfoPacketLength);
//                infoPacket = InfoPacket.Parser.ParseFrom(bufferInfoPacket);
//                //ParseDataField(infoPacket.Type, infoPacket.Data);
//            }

//            var bufferPayload = new byte[header.PayloadLength];

//            if (header.PayloadLength != 0)
//            {
//                Array.Copy(data, MessageHeader.BinarySize + header.InfoPacketLength, bufferPayload, 0, header.PayloadLength);

//                //сделать Task
//                //// запись в файл
//                //using (FileStream fstream = new FileStream($".\note.txt", FileMode.OpenOrCreate))
//                //{
//                //    for (long i = 0; i < header.PayloadLength; i++)
//                //    {
//                //        fstream.WriteByte(bufferPayload[i]);
//                //    }
//                //    //// запись массива байтов в файл
//                //    //fstream.Write(bufferPayload, 0, bufferPayload.);
//                //    Console.WriteLine("Текст записан в файл");
//                //}
//            }
//            ParseDataField(infoPacket.Type, infoPacket.Data, infoPacket, bufferPayload);
//            return infoPacket;
//        }


//        private EventHandler<VereskEvent> OnGetVereskEvent;
//        private EventHandler<VereskResult> OnGetVereskResult;
//        private EventHandler<OperationsInstructionsReport> OnGetOperationsInstructionsReport;

//        private void ParseDataField(uint type, ByteString data, InfoPacket infoPacket, byte[] payload)
//        {
//            switch ((InfoPacket.Types.PacketType)type)
//            {
//                case InfoPacket.Types.PacketType.VereskEvent:
//                    var ev = VereskEvent.Parser.ParseFrom(data);
//                    OnGetVereskEvent?.Invoke(this, ev);
//                    break;
//                case InfoPacket.Types.PacketType.VereskResult:
//                    var res = VereskResult.Parser.ParseFrom(data);
//                    OnGetVereskResult?.Invoke(this, res);
//                    break;
//                case InfoPacket.Types.PacketType.Notification:
//                    //return Notification.Parser.ParseFrom(data);
//                    break;
//                case InfoPacket.Types.PacketType.FileFragment:
//                    var fragment = FileFragment.Parser.ParseFrom(data);
//                    try
//                    {
//                        // Записываем uuid группы заданий в файл, чтобы потом можно было
//                        // прочитать его, когда нам понадобится её удалить
//                        using (FileStream fstream = new FileStream($".\\records\\" + UuidToString(fragment.Uuid) + "_" + fragment.Filename, FileMode.OpenOrCreate))
//                        {
//                            fstream.Write(payload);
//                            Console.WriteLine("Текст записан в файл");
//                        }
//                    }
//                    catch (Exception ex) { }
//                    System.Threading.Tasks.Task.Run(() => SendRequestNoWaiting(new InfoPacket() { Id = infoPacket.Id, Type = (uint)InfoPacket.Types.PacketType.Notification })); ;
//                    break;
//                case InfoPacket.Types.PacketType.OperationsInstructionsReport:
//                    var oiReport = OperationsInstructionsReport.Parser.ParseFrom(data);
//                    break;
//                default:
//                    break;
//            }
//        }
    
//    #endregion

//    #region Commands

//    ///// <summary>
//    ///// Connect to server
//    ///// </summary>
//    //public async Task<bool> Connect(string ipAddress, int port, double timeoutMilliseconds = 3000)
//    //{
//    //    if (_clientTcp != null)
//    //        Disconnect();

//    //    bool result = false;

//    //    try
//    //    {
//    //        _clientTcp = new ClientTcp();

//    //        SubscribeToClientTcpEvents();

//    //        await System.Threading.Tasks.Task.Run(async () =>
//    //        {
//    //            result = await _clientTcp.Connect(ipAddress, port, timeoutMilliseconds);
//    //        });
//    //    }
//    //    catch { }

//    //    return result;
//    //}

//    //private void SubscribeToClientTcpEvents()
//    //{
//    //    _clientTcp.OnConnect += (sender, args) => OnConnect?.Invoke(this, args);
//    //    _clientTcp.OnDisconnect += (sender, args) => OnDisconnect?.Invoke(this, args);
//    //    _clientTcp.OnWriteBytes += (sender, args) => OnWriteBytes?.Invoke(this, args);
//    //    _clientTcp.OnReadBytes += (sender, args) => OnReadBytes?.Invoke(this, args);
//    //}

//    ////private void UnsubscribeToClientTcpEvents()
//    ////{
//    ////    _clientTcp.OnConnect -= (sender, args) => OnConnect?.Invoke(this, args);
//    ////    _clientTcp.OnDisconnect -= (sender, args) => OnDisconnect?.Invoke(this, args);
//    ////}

//    ///// <summary>
//    ///// Disconnect from server
//    ///// </summary>
//    //public void Disconnect()
//    //{
//    //    if (_clientTcp == null)
//    //        return;

//    //    _clientTcp.Disconnect(); //TODO: проверить, отпишется ли от событий сам
//    //    _clientTcp = null;
//    //}

//    ////private async Task<(InfoPacket, byte[])> SendRequest(InfoPacket message)
//    ////{
//    ////    var messageBytes = message.ToByteArray();
//    ////    MessageHeader header = new MessageHeader(messageBytes.Length, 0, 0);


//    ////    var bSend = header.GetBytes().Concat(messageBytes).ToArray();

//    ////    return await _clientTcp.Send(bSend);
//    ////}

//    //private async Task<bool> SendRequest(InfoPacket message)
//    //{
//    //    var messageBytes = message.ToByteArray();
//    //    MessageHeader header = new MessageHeader(messageBytes.Length, 0, 0);


//    //    var bSend = header.GetBytes().Concat(messageBytes).ToArray();

//    //    return await _clientTcp.Send(bSend);
//    //}

//    private static uint _counter = 0;
//    /// <summary>
//    ///  Возвращает новый идентификатор пакета
//    /// :return: Целое число - идентификатор пакета
//    /// </summary>
//    private uint GetNextPacketId() {
//        _counter += 1;
//        return _counter - 1;
//    }

//    public async void AddTask()
//    {
//        try
//        {
//            List<Rdmp.Veresk.Task> tasks = new List<Rdmp.Veresk.Task>();
//            tasks.Add(CreateCommonVereskTask("Задание от " + DateTime.Now.ToString("dd-mm-yy HH:mm:ssS"), false));
//            (InfoPacket, Uuid) information = CreateInfoPacketWithTasks(tasks);
//            LastUuid = information.Item2;

//            try
//            {
//                // Записываем uuid группы заданий в файл, чтобы потом можно было
//                // прочитать его, когда нам понадобится её удалить
//                using (FileStream fstream = new FileStream($".\\task_uuid.txt", FileMode.OpenOrCreate))
//                {
//                    fstream.Write(information.Item2.ToByteArray()); //вообще надо по-другому записывать, но пока похуй
//                                                                    //// запись массива байтов в файл
//                                                                    //fstream.Write(bufferPayload, 0, bufferPayload.);
//                    Console.WriteLine("Текст записан в файл");
//                }
//            }
//            catch (Exception ex) { }

//                //var answer = SendRequest(information.Item1);
//              SendRequestNoWaiting(information.Item1);

//            }
//        catch (Exception e)
//        {
//            //return false;
//            throw new Exception(e.Message);
//        }
//    }

//    public async void DeleteTask()
//    {
//        try
//        {
//            var del_task_packet = CreateDeleteTaskRequest(LastUuid);


//            var answer = SendRequest(del_task_packet);
//        }
//        catch (Exception e)
//        {
//            //return false;
//            throw new Exception(e.Message);
//        }
//    }





//    private Uuid GetUuid(int id = -1)
//    {
//        //var u = new Uuid();
//        var temp = System.Guid.NewGuid().ToByteArray();

//        byte[] ub;
//        if (id != -1)
//        {
//            ub = temp.Take(12).ToArray().Concat(BitConverter.GetBytes(id)).ToArray(); //проверить порядок байт
//        }
//        else
//        {
//            ub = temp;
//        }

//        var p1 = BitConverter.ToUInt64(ub.Take(8).ToArray());
//        var p2 = BitConverter.ToUInt64(ub.TakeLast(8).ToArray());
//        var uuid_obj = new Uuid();
//        uuid_obj.P1 = p1;
//        uuid_obj.P2 = p2;
//        return uuid_obj;
//    }

//    private (InfoPacket, Uuid) CreateInfoPacketWithTasks(List<Rdmp.Veresk.Task> task_vec)
//    {
//        var oi = new OperationsInstructions();
//        oi.Id = GetUuid();
//        oi.Type = (uint)OperationsInstructions.Types.Type.CreateOrUpdate;
//        oi.Title = "Группа заданий №1";
//        //oi.Duration
//        foreach (var vt in task_vec)
//        {
//            var roi = new Roi();
//            roi.Id = GetUuid();

//            var roi_ins = new RoiInstructions();
//            roi_ins.Roi = roi;


//            var ot = new OperationTask();
//            ot.SpecificTaskType = (uint)OperationTask.Types.SpecificTaskType.Veresk;
//            ot.SpecificTask = vt.ToByteString();
//            roi_ins.OperationTasks.Add(ot);

//            oi.Instructions.Add(roi_ins);
//        }

//        var ip = new InfoPacket();
//        ip.Type = (uint)InfoPacket.Types.PacketType.OperationsInstructions;
//        ip.Id = GetNextPacketId();
//        ip.Data = oi.ToByteString();

//        return (ip, oi.Id);
//    }


//    /// <summary>
//    ///Создает новое задание для Вереска для поиска сигналов с фазовой манипуляцией, а также ALE - 2G
//    ///: param name: str - имя нового задания
//    ///:param is_all_dets: bool -флаг, требующий включить в задание все обнаружители
//    ///:return Veresk_common_pb2.Task: Объект типа Veresk_common_pb2.Task
//    /// </summary>
//    private Rdmp.Veresk.Task CreateCommonVereskTask(string name, bool is_all_dets) {


//        // Для начала создаем объект задания и заполняем поля его имени и типа (поиск/наблюдение)
//        var vt = new Rdmp.Veresk.Task();
//        vt.TaskName = name;
//        vt.TaskType = Rdmp.Veresk.Task.Types.TaskType.TtSearch;
//        // Задание на мониторинг (отдельных частот) нужно создавать вот так:
//        //vt.task_type = Veresk_common_pb2.Task.TT_MONITORING

//        // Теперь заполним классы сигналов, которые нужно обнаруживать в задании
//        vt.SigClasses.AddRange(new List<SignalClass>{SignalClass.ScAle2G,
//                                   SignalClass.ScAle3G,
//                                   SignalClass.ScSt4529,
//                                   SignalClass.ScMil110A,
//                                   SignalClass.ScMil110BAppc,
//                                   SignalClass.ScMil110CAppd,
//                                   SignalClass.ScDc7200,
//                                   SignalClass.ScLink22,
//                                   SignalClass.ScB162 });

//        // Включим сомнительный с точки зрения необходимости обнаружитель ST4285, если включен флаг.
//        if (is_all_dets)
//        { vt.SigClasses.Add(SignalClass.ScSt4285); }

//        // Не надо нам посылать запросы на пеленгатор, он и так через БД увидит нашу работу
//        // и подпишет пеленги
//        vt.RequestDirections = false;
//        vt.RequestLocations = false;

//        // Использовать оперативный откат - хорошая идея для заданий на поиск
//        vt.UseFastRollback = true;

//        // Добавим диапазон частот 1.5 ... 30 МГц в задание с шагом в 2.5 кГц
//        var default_freq_range = new FreqRange();
//        default_freq_range.StartFreq = 1500000;
//        default_freq_range.EndFreq = 30000000;
//        default_freq_range.Delta = 2500;

//        vt.FreqRanges.Add(default_freq_range);
//        // Можно вот так добавить отдельные частоты (например, в задание для мониторинга):
//        // vt.freqs.append(9795000)
//        // vt.freqs.append(7845000)

//        // Запросим, например, 500 каналов для этого задания
//        vt.RequestedChannels = 500;

//        return vt;
//    }


//    /// <summary>
//    ///Удаляет задание или группу заданий Вереска
//    ///:param task_uuid: object -уникальный идентификатор задания, которое нужно удалить
//    ///:return object: Объект типа InfoPacket, содержащий внутри запрос на удаление задания
//    /// </summary>
//    private InfoPacket CreateDeleteTaskRequest(Uuid uuid) {
//        var oi = new OperationsInstructions();
//        oi.Id = uuid;
//        oi.Type = (uint)OperationsInstructions.Types.Type.Cancel;

//        var ip = new InfoPacket();
//        ip.Type = (uint)InfoPacket.Types.PacketType.OperationsInstructions;
//        ip.Id = GetNextPacketId();
//        ip.Data = oi.ToByteString();

//        return ip;
//    }

//    /// <summary>
//    /// Конвертирует объект типа Rdmp.Uuid в строку
//    ///:param uuid: объект типа Rdmp.Uuid
//    ///:return: строка с шестнадцатиричным представлением uuid
//    /// </summary>
//    private string UuidToString(Uuid uuid)
//    {
//        var b1 = Convert.ToHexString(BitConverter.GetBytes(uuid.P1));
//        var b2 = Convert.ToHexString(BitConverter.GetBytes(uuid.P2));

//        return b1 + b2;
//    }

        


//    #endregion
    }
}
