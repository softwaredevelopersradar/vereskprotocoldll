﻿using llcss;
using System;

namespace VereskClientDll
{
    public struct MessageHeader
    {
        public int InfoPacketLength;
        public long PayloadLength;
        public short Version;

        public const int BinarySize = 14;
        public int StructureBinarySize { get { return BinarySize; } }

        public MessageHeader(int infoPacketLength, long payloadLength, short version)
        {
            InfoPacketLength = infoPacketLength;
            PayloadLength = payloadLength;
            Version = version;
        }

        public static byte[] ToBinary(int infoPacketLength, long payloadLength, short version)
        {
            return new MessageHeader(infoPacketLength, payloadLength, version).GetBytes();
        }

        public void Decode(byte[] buffer, int shiftIndex)
        {
            InfoPacketLength = SerializationExtensions.Decode32(buffer, shiftIndex); 
            shiftIndex += 4;

            PayloadLength = SerializationExtensions.Decode64(buffer, shiftIndex); 
            shiftIndex += 8;

            Version = SerializationExtensions.Decode16(buffer, shiftIndex);
            shiftIndex += 2;
        }

        public byte[] GetBytes()
        {
            var buffer = new byte[StructureBinarySize];
            GetBytes(buffer, 0);
            return buffer;
        }
        

        public bool TryGetBytes(out byte[] result)
        {
            try
            {
                result = GetBytes();
                return true;
            }
            catch
            {
                result = null;
                return false;
            }
        }

        public void GetBytes(byte[] buffer, int shiftIndex)
        {
            SerializationExtensions.GetBytes(InfoPacketLength, 4, buffer, shiftIndex);
            shiftIndex += 4;

            SerializationExtensions.GetBytes(PayloadLength, 8, buffer, shiftIndex);
            shiftIndex += 8;

            SerializationExtensions.GetBytes(Version, 2, buffer, shiftIndex);
            shiftIndex += 2;
        }
        

        public static bool IsValid(byte[] buffer, int startIndex = 0)
        {
            return IsValid(buffer, ref startIndex);
        }

        public static bool IsValid(byte[] buffer, ref int startIndex)
        {
            if (buffer == null)
            {
                return false;
            }
            if (buffer.Length < startIndex + BinarySize)
            {
                return false;
            }
            startIndex += 4;

            startIndex += 8;

            startIndex += 2;

            return true;
        }

        public static MessageHeader Parse(byte[] buffer, int startIndex = 0, bool validate = false)
        {
            if (validate && !IsValid(buffer))
            {
                throw new ArgumentException("buffer parse error");
            }
            var result = new MessageHeader();
            result.Decode(buffer, startIndex);
            return result;
        }

        public static bool TryParse(byte[] buffer, out MessageHeader result)
        {
            if (!IsValid(buffer))
            {
                result = default(MessageHeader);
                return false;
            }
            try
            {
                result = new MessageHeader();
                result.Decode(buffer, 0);
                return true;
            }
            catch
            {
                result = default(MessageHeader);
                return false;
            }
        }
    }
}
