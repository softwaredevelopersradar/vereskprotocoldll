﻿using Rdmp;
using System;
using Google.Protobuf;
using VereskClientDll;
using System.Threading.Tasks;

namespace TestConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            //var temp1 = new GeoCoordinates() { Latitude = 53, Longitude = 24, Altitude = 300, Epsg = 1 };
            //var temp2 = temp1.ToByteArray();
            //Console.WriteLine(temp2);
            //var temp3 = GeoCoordinates.Parser.ParseFrom(temp2);
            Task.Run(() => StartClient());

            Console.ReadLine();

        }

        private static async Task StartClient()
        { 
            var vereslClient = new VereskClient();
            vereslClient.Connect("86.57.245.94", 32133);
        }
    }
}
