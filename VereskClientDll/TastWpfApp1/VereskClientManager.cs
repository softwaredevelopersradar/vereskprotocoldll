﻿using Rdmp.Veresk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VereskClientDll;

namespace TastWpfApp1
{
    public class VereskClientManager
    {
        private readonly VereskClientTcp _client;
        public bool IsConnected { get; private set; }

        public event EventHandler<string> OnMessToMain;
        public event EventHandler OnDisconnected;
        public event EventHandler OnConnected;

        public VereskClientManager()
        {
            _client = new VereskClientTcp();
        }

        #region Commands
        public async System.Threading.Tasks.Task ConnectAsync(string serverIp, int serberPort)
        {
            if (IsConnected)
                return;
            SubscribeToLibEvents();

            _client.Connect(serverIp, serberPort);
        }


        public void Disconnect()
        {
            if (!IsConnected)
                return;

            _client.Disconnect();

        }


        public void AddTask()
        {
            if (!IsConnected)
                return;

            _client.AddTask();
        }

        public void AddTaskWithParams()
        {
            if (!IsConnected)
                return;
            var signals = new List<SignalClass>() {SignalClass.ScAle2G,
                                   SignalClass.ScAle3G,
                                   SignalClass.ScSt4529,
                                   SignalClass.ScMil110A,
                                   SignalClass.ScMil110BAppc,
                                   SignalClass.ScMil110CAppd,
                                   SignalClass.ScDc7200,
                                   SignalClass.ScLink22,
                                   SignalClass.ScB162, SignalClass.ScSt4285};
            var freqRanges = new List<FreqRange>() { new FreqRange() { StartFreq = 1500000,EndFreq = 30000000,Delta = 2500 }
            };
            var freqs = new List<uint>() { 9795000, 7845000 };
            var answer = (Rdmp.OperationsInstructionsReport.Types.Type)_client.AddTask(signals, freqRanges, freqs).Result.Type;
        }

        public void DeleteTask()
        {
            if (!IsConnected)
                return;

            var answer = (Rdmp.OperationsInstructionsReport.Types.Type)_client.DeleteTask().Result.Type;
        }
        
        #endregion


        public void SubscribeToLibEvents()
        {
            _client.OnConnect += _client_OnConnect;
            _client.OnDisconnect += _client_OnDisconnect;
            _client.OnWriteBytes += _client_OnWriteBytes;
            _client.OnReadBytes += _client_OnReadBytes;
            _client.OnGetVereskEvent += _client_OnGetVereskEvent;
            _client.OnGetVereskFileFragment += _client_OnGetVereskFileFragment; ;
            _client.OnGetVereskNotification += _client_OnGetVereskNotification; ;
            _client.OnGetVereskResult += _client_OnGetVereskResult;
            //_client.Logger += ShowClientLogger;
            _client.OnGetOperationsInstructionsReport += _client_OnGetOperationsInstructionsReport;
        }

        private void ShowClientLogger(string message)
        {
            OnMessToMain(this, message);
        }

        private void _client_OnGetOperationsInstructionsReport(object sender, Rdmp.OperationsInstructionsReport e)
        {
            OnMessToMain(sender, "Got OI_Report, type = " + (Rdmp.OperationsInstructionsReport.Types.Type)e.Type + ", description: " + e.Description );
        }

        private void _client_OnGetVereskResult(object sender, VereskResult e)
        {
            OnMessToMain(sender, "Got VereskResult, freq = " + e.ProcSessionInfo.Freq);
        }

        private void _client_OnGetVereskNotification(object sender, EventArgs e)
        {
            OnMessToMain(sender, "Got Notification");
        }

        private void _client_OnGetVereskFileFragment(object sender, Rdmp.FileFragment e)
        {
            OnMessToMain(sender, "Got FileFragment, name = " + e.Filename);
        }

        private void _client_OnGetVereskEvent(object sender, VereskEvent e)
        {
            switch (e.EventType)
            {
                case VereskEventType.VetOpenedSession:
                    OnMessToMain(sender, "Got VereskEvent, freq = " + e.OpenSess?.DetectFreq + ", signal class = " + e.OpenSess.SigClass);
                    break;
                case VereskEventType.VetClosedSession:
                    OnMessToMain(sender, "Got VereskEvent, freq = " + e.CloseSess?.SessFreq + ", signal class = " + e.CloseSess.SigClassesList.FirstOrDefault());
                    break;
                case VereskEventType.VetSignalDetected:
                    OnMessToMain(sender, "Got VereskEvent, freq = " + e.SigDet?.DetectFreq + ", signal class = " + e.SigDet.SigClass);
                    break;
                default:
                    break;
            }
            
        }

        public void UnsubscribeFromLibEvents()
        {
            _client.OnConnect -= _client_OnConnect;
            _client.OnDisconnect -= _client_OnDisconnect;
            _client.OnWriteBytes -= _client_OnWriteBytes;
            _client.OnReadBytes -= _client_OnReadBytes;
            _client.OnGetVereskFileFragment -= _client_OnGetVereskFileFragment; ;
            _client.OnGetVereskNotification -= _client_OnGetVereskNotification; ;
            _client.OnGetVereskResult -= _client_OnGetVereskResult;
            _client.OnGetOperationsInstructionsReport -= _client_OnGetOperationsInstructionsReport;
        }


        #region EventHandlers

        private void _client_OnReadBytes(object sender, byte[] e)
        {
            OnMessToMain(sender, "Get " + e.Length + " bytes from server");
        }

        private void _client_OnWriteBytes(object sender, byte[] e)
        {
            OnMessToMain(sender, "Send " + e.Length + " bytes to server");
        }

        private void _client_OnDisconnect(object sender, EventArgs e)
        {
            IsConnected = false;
            OnMessToMain(sender, "Disconnected from Veresk server");
            OnDisconnected?.Invoke(this, null);
            UnsubscribeFromLibEvents();
        }

        private void _client_OnConnect(object sender, EventArgs e)
        {
            IsConnected = true;
            OnMessToMain(sender, "Connected to Veresk server");
            OnConnected?.Invoke(this, null);
        }


        #endregion

        //private void OnLostGenerator(object sender, EventArgs e)
        //{
        //    OnMessToMain(sender, "USRP server is lost");
        //}


        //private void OnSendCmd(object sender, byte code)
        //{
        //    OnMessToMain(sender, "Send command " + ((CodesUsrp)code).ToString());
        //}
    }
}
