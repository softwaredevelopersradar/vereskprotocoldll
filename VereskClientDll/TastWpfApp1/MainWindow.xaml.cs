﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VereskClientDll;
using GrpcClientLib;
using InheritorsEventArgs;
using KvetkaModelsDBLib;
using System.Threading;

namespace TastWpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        VereskClientManager clientVereskManager;
        ServiceClient clientDB; 
        string text = "";

        public string TextMess
        {
            get { return text; }
            set
            {
                text = value;
                PropertyChanged(this, new PropertyChangedEventArgs(nameof(TextMess)));
            }
        }


        public MainWindow()
        {
            DataContext = this;
            InitializeComponent();

            clientVereskManager = new VereskClientManager();
            SubscribeToManagerEvents();
        }

        private void SubscribeToManagerEvents()
        {
            clientVereskManager.OnMessToMain += ClientManager_OnMessToMain; 
            clientVereskManager.OnConnected += ClientManager_OnConnected;
            clientVereskManager.OnDisconnected += ClientManager_OnDisconnected; 
        }

        private void ClientManager_OnDisconnected(object sender, EventArgs e)
        {
            DispatchIfNecessary(() =>
            {
                Connection.Background = Brushes.Red;
            });
            
            //throw new NotImplementedException();
        }

        private void ClientManager_OnConnected(object sender, EventArgs e)
        {
            DispatchIfNecessary(() =>
            {
                Connection.Background = Brushes.Green;
            });
            //throw new NotImplementedException();
        }

        private void ClientManager_OnMessToMain(object sender, string e)
        {
            TextMess += e + "\n" + "\n";
        }

        

       

        private void Connection_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.ConnectAsync(tbIp.Text, Convert.ToInt32(tbPort.Text));
        }

        private void Disconnection_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.Disconnect();
        }

        private void BtnSendTask_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.AddTask();
        }

        private void BtnDeleteTask_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.DeleteTask();
        }

        
        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            TextMess = "";
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            System.Windows.Threading.Dispatcher.ExitAllFrames();
        }


        #region DB
        private void ConnectionDB_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (clientDB != null)
                    return;
                else
                {
                    clientDB = new ServiceClient(this.Name, tbIpDb.Text, Convert.ToInt32(tbPortDb.Text));
                    InitClientDB();
                    clientDB.Connect();
                }
            }
            catch (ExceptionClient exceptClient)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }

        private void DisconnectionDB_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (clientDB != null)
                    clientDB.Disconnect();
            }
            catch (ExceptionClient exceptClient)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }

        private void InitClientDB()
        {
            clientDB.OnConnect += ClientDB_OnConnect;
            clientDB.OnDisconnect += ClientDB_Disconnect;
            //clientDB.OnUpData += HandlerUpData;
            //(clientDB.Tables[NameTable.TableJammer] as ITableUpdate<TableJammer>).OnUpTable += MainWindow_OnUpTable;
        }

        private async void ClientDB_OnConnect(object sender, ClientEventArgs e)
        {
            ConnectionDB.Background = Brushes.Green;
            //foreach (NameTable table in Enum.GetValues(typeof(NameTable)))
            //{
            //    var tempTable = await clientDB.Tables[table].LoadAsync<AbstractCommonTable>();
            //    DispatchIfNecessary(() =>
            //    {
            //        tbMessage.AppendText($"Load data from Db. {table.ToString()} count records - {tempTable.Count} \n");
            //    });
            //}
        }

        void ClientDB_Disconnect(object obj, ClientEventArgs eventArgs)
        {
            ConnectionDB.Background = Brushes.Red;
            if (eventArgs.GetMessage != "")
            {
                //tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(eventArgs.GetMessage);
            }
            clientDB = null;
        }


        void WriteResultToDB(Rdmp.Veresk.VereskResult result)
        {
            var recordDb = new TableResFF();
            recordDb.FrequencyKHz = result.ProcSessionInfo.Freq/1000;
            recordDb.ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>(){
               new TableTrackResFF(){ }
            };
            recordDb.Note = result.ProcSessionInfo.SigClassesList.FirstOrDefault().SignalClassName;
            clientDB?.Tables[NameTable.TableResFF].Add(recordDb);
        }

        void WriteResultToDB(Rdmp.Veresk.VereskEvent result)
        {
            var recordDb = new TableResFF();
            if (result.EventType == Rdmp.Veresk.VereskEventType.VetOpenedSession)
            {
                recordDb.FrequencyKHz = result.OpenSess.DetectFreq / 1000;
                recordDb.ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>(){
                    new TableTrackResFF(){ }
                };
                recordDb.Note = result.OpenSess.SigClass.SignalClassName;
                recordDb.Time = DateTimeOffset.FromUnixTimeMilliseconds((long)result.OpenSess.StartTimestamp).DateTime; 
                clientDB?.Tables[NameTable.TableResFF].Add(recordDb);
            }
        }

        //void HandlerUpData(object obj, DataEventArgs eventArgs)
        //{
        //    Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
        //    {
        //        tbMessage.Foreground = System.Windows.Media.Brushes.Black;

        //        tbMessage.AppendText($"Load data from Db. Tables {eventArgs.Name.ToString()} count records - {eventArgs.AbstractData.ListRecords.Count} \n");
        //    });
        //}

        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        #endregion


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        private void btnSendTaskParams_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.AddTaskWithParams();
        }
    }
}
