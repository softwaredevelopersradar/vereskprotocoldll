# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: InfoPacket.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


import GeoData_pb2 as GeoData__pb2
import RouteData_pb2 as RouteData__pb2
import TargetingData_pb2 as TargetingData__pb2
import VisualData_pb2 as VisualData__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='InfoPacket.proto',
  package='Rdmp',
  syntax='proto2',
  serialized_pb=_b('\n\x10InfoPacket.proto\x12\x04Rdmp\x1a\rGeoData.proto\x1a\x0fRouteData.proto\x1a\x13TargetingData.proto\x1a\x10VisualData.proto\"\xd6\x11\n\nInfoPacket\x12\x0c\n\x04type\x18\x01 \x02(\r\x12\n\n\x02id\x18\x02 \x02(\r\x12\x0c\n\x04\x64\x61ta\x18\x03 \x01(\x0c\"\x9f\x11\n\nPacketType\x12\x10\n\x0cNotification\x10\x01\x12\n\n\x06Target\x10\x02\x12\n\n\x06Pixmap\x10\x03\x12\t\n\x05Video\x10\x04\x12\x0e\n\nFlightPlan\x10\x05\x12\t\n\x05Route\x10\x06\x12\r\n\tTelemetry\x10\x07\x12\x13\n\x0f\x44iscoveryBeacon\x10\x08\x12\x1a\n\x16OperationsInstructions\x10\t\x12\x15\n\x11\x41irspaceStructure\x10\n\x12\x17\n\x13\x44iscoveryBeaconLite\x10\x0b\x12 \n\x1cOperationsInstructionsReport\x10\x0c\x12\x1a\n\x16\x44iscoveryBeaconRequest\x10\r\x12\x10\n\x0c\x46ileFragment\x10\x0e\x12\x0f\n\x0b\x46ileRequest\x10\x0f\x12\x14\n\x10PhotoPlanRequest\x10\x10\x12\x15\n\x11PhotoPlanResponse\x10\x11\x12\x0e\n\nRtrMessage\x10\x12\x12\x0f\n\x0b\x46ortisState\x10\x13\x12\x11\n\rAttackRlsTask\x10\x14\x12\x17\n\x13\x41ttackRlsTaskAnswer\x10\x15\x12\x11\n\rSearchRlsTask\x10\x16\x12\x13\n\x0fSearchRlsAnswer\x10\x17\x12\x0e\n\x08KtpState\x10\x81\x80@\x12\x0e\n\x08\x45wfRecon\x10\x81\xa0@\x12\x13\n\rEwfUserAction\x10\x82\xa0@\x12\x17\n\x11\x45wfServiceMessage\x10\x83\xa0@\x12\x17\n\x11\x45wfRemoteCommands\x10\x84\xa0@\x12\x1a\n\x14\x45wfRemoteCommandsAck\x10\x85\xa0@\x12\x16\n\x10\x45wfRemoteNetwork\x10\x86\xa0@\x12\x1f\n\x19SatelliteSuppressionRecon\x10\x81\xc0@\x12/\n)SatelliteSuppressionAvailableModesRequest\x10\x82\xc0@\x12\x30\n*SatelliteSuppressionAvailableModesResponse\x10\x83\xc0@\x12\'\n!SatelliteSuppressionExtendedRecon\x10\x84\xc0@\x12!\n\x1bSatelliteStartImitationTask\x10\x85\xc0@\x12#\n\x1dSatelliteStartSuppressionTask\x10\x86\xc0@\x12\x17\n\x11SatelliteStopTask\x10\x87\xc0@\x12\x18\n\x12SatelliteTaskReply\x10\x88\xc0@\x12\x1c\n\x16SatellitePayloadStatus\x10\x89\xc0@\x12\x1c\n\x16NpuCameraFlightRequest\x10\x81\xe0@\x12\x1d\n\x17NpuCameraFlightResponse\x10\x82\xe0@\x12\x1c\n\x16NpuCameraFlightPointer\x10\x83\xe0@\x12\x13\n\rNpuShowMarker\x10\x84\xe0@\x12\"\n\x1cNpuCameraFlightStatusRequest\x10\x85\xe0@\x12\x18\n\x12GetPositionRequest\x10\x81\x80\x41\x12\x19\n\x13GetPositionResponse\x10\x82\x80\x41\x12\x19\n\x13\x46ireSupportResponse\x10\x85\xa0\x41\x12\x0f\n\tBurstInfo\x10\x86\xa0\x41\x12\x12\n\x0c\x46lightStatus\x10\x87\xa0\x41\x12\x1b\n\x15\x46ireAdjustmentRequest\x10\x88\xa0\x41\x12\x1a\n\x14\x46ireAdjustmentAnswer\x10\x89\xa0\x41\x12\x12\n\x0cMeetingPoint\x10\x90\xa0\x41\x12\x1d\n\x17MissionResourcesMessage\x10\x85\xc0\x41\x12\x15\n\x0f\x46ireSupportTask\x10\x82\x80\x44\x12\x12\n\x0cMeteoRequest\x10\x86\x80\x44\x12\x16\n\x10MeteoAvgResponse\x10\x87\x80\x44\x12\x18\n\x12MeteoSituationInfo\x10\x88\x80\x44\x12\x13\n\rMeteoResponse\x10\x89\x80\x44\x12\x15\n\x0e\x41vgWindRequest\x10\x90\x80\xc0\x08\x12\x15\n\x0f\x41vgWindResponse\x10\x91\x80\x44\x12\x15\n\x0fInformalMessage\x10\x92\x80\x44\x12\x16\n\x10GeoMeteoResponse\x10\x94\x80\x44\x12\x14\n\rUhfSourceInfo\x10\x81\xa0\x80\x01\x12\x11\n\nUhfEesInfo\x10\x82\xa0\x80\x01\x12\x14\n\rRtrSourceInfo\x10\x83\xa0\x80\x01\x12\x14\n\rRloSourceInfo\x10\x84\xa0\x80\x01\x12\x14\n\rAisSourceInfo\x10\x85\xa0\x80\x01\x12\x11\n\nActive3gpp\x10\x81\xc0\x80\x01\x12\x14\n\rActiveShelest\x10\x85\xc0\x80\x01\x12\x1b\n\x14RadarDetectorMessage\x10\x86\xc0\x80\x01\x12\x13\n\rEphemerisMask\x10\x81\x80H\x12\x13\n\rEphemerisData\x10\x82\x80H\x12\x1a\n\x14RequestEphemerisData\x10\x83\x80H\x12\x11\n\x0b\x41lmanacMask\x10\x84\x80H\x12\r\n\x07\x41lmanac\x10\x85\x80H\x12\x13\n\x0cVereskResult\x10\x81\xe0\x80\x01\x12\x19\n\x12\x44\x66PostTypeValueMin\x10\x80\xc0\x81\x01\x12\x19\n\x12\x44\x66PostRegistration\x10\x81\xc0\x81\x01\x12\x1a\n\x13\x44\x66\x43onnectionControl\x10\x82\xc0\x81\x01\x12\x18\n\x11\x44\x66SimpleACARSTask\x10\x83\xc0\x81\x01\x12\x18\n\x11\x44\x66SimpleGMDSSTask\x10\x84\xc0\x81\x01\x12\x18\n\x11\x44\x66SimpleVDLm2Task\x10\x85\xc0\x81\x01\x12\x1a\n\x13\x44\x66SimpleTaskControl\x10\x86\xc0\x81\x01\x12\x19\n\x12\x44\x66SimpleTaskStatus\x10\x87\xc0\x81\x01\x12\x15\n\x0e\x44\x66SimpleResult\x10\x88\xc0\x81\x01\x12\x1b\n\x14\x44\x66TechnicalStateInfo\x10\x89\xc0\x81\x01\x12\x15\n\x0e\x44\x66SWTaskParams\x10\x90\xc0\x81\x01\x12\x16\n\x0f\x44\x66SWTaskResults\x10\x91\xc0\x81\x01\x12\x19\n\x12\x44\x66PostTypeValueMax\x10\x99\xd3\x81\x01\x12\x12\n\x0bPbDocStruct\x10\x81\xa0\xe6\x04\x12\x12\n\x0bPbDocNotify\x10\x82\xa0\xe6\x04\x42\x1f\n\x0bru.stc.rdmpB\x0eRdmpInfoPacketH\x03')
  ,
  dependencies=[GeoData__pb2.DESCRIPTOR,RouteData__pb2.DESCRIPTOR,TargetingData__pb2.DESCRIPTOR,VisualData__pb2.DESCRIPTOR,])
_sym_db.RegisterFileDescriptor(DESCRIPTOR)



_INFOPACKET_PACKETTYPE = _descriptor.EnumDescriptor(
  name='PacketType',
  full_name='Rdmp.InfoPacket.PacketType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='Notification', index=0, number=1,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='Target', index=1, number=2,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='Pixmap', index=2, number=3,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='Video', index=3, number=4,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FlightPlan', index=4, number=5,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='Route', index=5, number=6,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='Telemetry', index=6, number=7,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DiscoveryBeacon', index=7, number=8,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='OperationsInstructions', index=8, number=9,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='AirspaceStructure', index=9, number=10,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DiscoveryBeaconLite', index=10, number=11,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='OperationsInstructionsReport', index=11, number=12,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DiscoveryBeaconRequest', index=12, number=13,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FileFragment', index=13, number=14,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FileRequest', index=14, number=15,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='PhotoPlanRequest', index=15, number=16,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='PhotoPlanResponse', index=16, number=17,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='RtrMessage', index=17, number=18,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FortisState', index=18, number=19,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='AttackRlsTask', index=19, number=20,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='AttackRlsTaskAnswer', index=20, number=21,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SearchRlsTask', index=21, number=22,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SearchRlsAnswer', index=22, number=23,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='KtpState', index=23, number=1048577,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EwfRecon', index=24, number=1052673,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EwfUserAction', index=25, number=1052674,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EwfServiceMessage', index=26, number=1052675,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EwfRemoteCommands', index=27, number=1052676,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EwfRemoteCommandsAck', index=28, number=1052677,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EwfRemoteNetwork', index=29, number=1052678,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SatelliteSuppressionRecon', index=30, number=1056769,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SatelliteSuppressionAvailableModesRequest', index=31, number=1056770,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SatelliteSuppressionAvailableModesResponse', index=32, number=1056771,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SatelliteSuppressionExtendedRecon', index=33, number=1056772,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SatelliteStartImitationTask', index=34, number=1056773,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SatelliteStartSuppressionTask', index=35, number=1056774,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SatelliteStopTask', index=36, number=1056775,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SatelliteTaskReply', index=37, number=1056776,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SatellitePayloadStatus', index=38, number=1056777,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpuCameraFlightRequest', index=39, number=1060865,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpuCameraFlightResponse', index=40, number=1060866,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpuCameraFlightPointer', index=41, number=1060867,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpuShowMarker', index=42, number=1060868,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NpuCameraFlightStatusRequest', index=43, number=1060869,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GetPositionRequest', index=44, number=1064961,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GetPositionResponse', index=45, number=1064962,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FireSupportResponse', index=46, number=1069061,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BurstInfo', index=47, number=1069062,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FlightStatus', index=48, number=1069063,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FireAdjustmentRequest', index=49, number=1069064,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FireAdjustmentAnswer', index=50, number=1069065,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='MeetingPoint', index=51, number=1069072,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='MissionResourcesMessage', index=52, number=1073157,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FireSupportTask', index=53, number=1114114,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='MeteoRequest', index=54, number=1114118,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='MeteoAvgResponse', index=55, number=1114119,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='MeteoSituationInfo', index=56, number=1114120,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='MeteoResponse', index=57, number=1114121,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='AvgWindRequest', index=58, number=17825808,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='AvgWindResponse', index=59, number=1114129,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='InformalMessage', index=60, number=1114130,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GeoMeteoResponse', index=61, number=1114132,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='UhfSourceInfo', index=62, number=2101249,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='UhfEesInfo', index=63, number=2101250,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='RtrSourceInfo', index=64, number=2101251,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='RloSourceInfo', index=65, number=2101252,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='AisSourceInfo', index=66, number=2101253,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='Active3gpp', index=67, number=2105345,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ActiveShelest', index=68, number=2105349,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='RadarDetectorMessage', index=69, number=2105350,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EphemerisMask', index=70, number=1179649,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EphemerisData', index=71, number=1179650,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='RequestEphemerisData', index=72, number=1179651,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='AlmanacMask', index=73, number=1179652,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='Almanac', index=74, number=1179653,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='VereskResult', index=75, number=2109441,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DfPostTypeValueMin', index=76, number=2121728,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DfPostRegistration', index=77, number=2121729,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DfConnectionControl', index=78, number=2121730,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DfSimpleACARSTask', index=79, number=2121731,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DfSimpleGMDSSTask', index=80, number=2121732,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DfSimpleVDLm2Task', index=81, number=2121733,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DfSimpleTaskControl', index=82, number=2121734,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DfSimpleTaskStatus', index=83, number=2121735,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DfSimpleResult', index=84, number=2121736,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DfTechnicalStateInfo', index=85, number=2121737,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DfSWTaskParams', index=86, number=2121744,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DfSWTaskResults', index=87, number=2121745,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DfPostTypeValueMax', index=88, number=2124185,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='PbDocStruct', index=89, number=10063873,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='PbDocNotify', index=90, number=10063874,
      options=None,
      type=None),
  ],
  containing_type=None,
  options=None,
  serialized_start=153,
  serialized_end=2360,
)
_sym_db.RegisterEnumDescriptor(_INFOPACKET_PACKETTYPE)


_INFOPACKET = _descriptor.Descriptor(
  name='InfoPacket',
  full_name='Rdmp.InfoPacket',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='type', full_name='Rdmp.InfoPacket.type', index=0,
      number=1, type=13, cpp_type=3, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='id', full_name='Rdmp.InfoPacket.id', index=1,
      number=2, type=13, cpp_type=3, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='data', full_name='Rdmp.InfoPacket.data', index=2,
      number=3, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
    _INFOPACKET_PACKETTYPE,
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=98,
  serialized_end=2360,
)

_INFOPACKET_PACKETTYPE.containing_type = _INFOPACKET
DESCRIPTOR.message_types_by_name['InfoPacket'] = _INFOPACKET

InfoPacket = _reflection.GeneratedProtocolMessageType('InfoPacket', (_message.Message,), dict(
  DESCRIPTOR = _INFOPACKET,
  __module__ = 'InfoPacket_pb2'
  # @@protoc_insertion_point(class_scope:Rdmp.InfoPacket)
  ))
_sym_db.RegisterMessage(InfoPacket)


DESCRIPTOR.has_options = True
DESCRIPTOR._options = _descriptor._ParseOptions(descriptor_pb2.FileOptions(), _b('\n\013ru.stc.rdmpB\016RdmpInfoPacketH\003'))
# @@protoc_insertion_point(module_scope)
