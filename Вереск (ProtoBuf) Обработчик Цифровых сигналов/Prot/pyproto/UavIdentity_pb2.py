# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: UavIdentity.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='UavIdentity.proto',
  package='Rdmp',
  syntax='proto2',
  serialized_pb=_b('\n\x11UavIdentity.proto\x12\x04Rdmp\"<\n\x0bUavIdentity\x12\n\n\x02id\x18\x01 \x02(\r\x12\x0c\n\x04type\x18\x02 \x01(\r\x12\x13\n\x0b\x64\x65scription\x18\x03 \x01(\t\"\x1e\n\x04Uuid\x12\n\n\x02p1\x18\x01 \x02(\x06\x12\n\n\x02p2\x18\x02 \x02(\x06*q\n\x07UavType\x12\r\n\x07Orlan10\x10\x80\x80@\x12\x0f\n\tOrlan10_U\x10\x81\x80@\x12\x0f\n\tOrlan10_C\x10\x82\x80@\x12\x14\n\rGroundStation\x10\x80\x80\x84P\x12\x11\n\nHelicopter\x10\x80\x80\x84x\x12\x0c\n\x04Mark\x10\x80\x80\x84\xf8\x01\x42 \n\x0bru.stc.rdmpB\x0fRdmpUavIdentityH\x03')
)
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

_UAVTYPE = _descriptor.EnumDescriptor(
  name='UavType',
  full_name='Rdmp.UavType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='Orlan10', index=0, number=1048576,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='Orlan10_U', index=1, number=1048577,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='Orlan10_C', index=2, number=1048578,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GroundStation', index=3, number=167837696,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='Helicopter', index=4, number=251723776,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='Mark', index=5, number=520159232,
      options=None,
      type=None),
  ],
  containing_type=None,
  options=None,
  serialized_start=121,
  serialized_end=234,
)
_sym_db.RegisterEnumDescriptor(_UAVTYPE)

UavType = enum_type_wrapper.EnumTypeWrapper(_UAVTYPE)
Orlan10 = 1048576
Orlan10_U = 1048577
Orlan10_C = 1048578
GroundStation = 167837696
Helicopter = 251723776
Mark = 520159232



_UAVIDENTITY = _descriptor.Descriptor(
  name='UavIdentity',
  full_name='Rdmp.UavIdentity',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='Rdmp.UavIdentity.id', index=0,
      number=1, type=13, cpp_type=3, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='type', full_name='Rdmp.UavIdentity.type', index=1,
      number=2, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='description', full_name='Rdmp.UavIdentity.description', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=27,
  serialized_end=87,
)


_UUID = _descriptor.Descriptor(
  name='Uuid',
  full_name='Rdmp.Uuid',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='p1', full_name='Rdmp.Uuid.p1', index=0,
      number=1, type=6, cpp_type=4, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='p2', full_name='Rdmp.Uuid.p2', index=1,
      number=2, type=6, cpp_type=4, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=89,
  serialized_end=119,
)

DESCRIPTOR.message_types_by_name['UavIdentity'] = _UAVIDENTITY
DESCRIPTOR.message_types_by_name['Uuid'] = _UUID
DESCRIPTOR.enum_types_by_name['UavType'] = _UAVTYPE

UavIdentity = _reflection.GeneratedProtocolMessageType('UavIdentity', (_message.Message,), dict(
  DESCRIPTOR = _UAVIDENTITY,
  __module__ = 'UavIdentity_pb2'
  # @@protoc_insertion_point(class_scope:Rdmp.UavIdentity)
  ))
_sym_db.RegisterMessage(UavIdentity)

Uuid = _reflection.GeneratedProtocolMessageType('Uuid', (_message.Message,), dict(
  DESCRIPTOR = _UUID,
  __module__ = 'UavIdentity_pb2'
  # @@protoc_insertion_point(class_scope:Rdmp.Uuid)
  ))
_sym_db.RegisterMessage(Uuid)


DESCRIPTOR.has_options = True
DESCRIPTOR._options = _descriptor._ParseOptions(descriptor_pb2.FileOptions(), _b('\n\013ru.stc.rdmpB\017RdmpUavIdentityH\003'))
# @@protoc_insertion_point(module_scope)
