﻿syntax = "proto2";

option optimize_for = LITE_RUNTIME;

import "Veresk_common.proto";

// Вереск: протокол взаимодействия с внешними сущностями (версия от 27.02.2017).
// Особенности работы по настоящему протоколу, инкапсулированному в ППРД, будут отдельно отмечены
// префиксом "ППРД:"

package Rdmp.veresk;

// ПРИМЕЧАНИЕ 1: Обобщенный тип сообщения - VereskMessage - определен в самом низу.
// ППРД: VereskMessage идет внутри поля specificTask пакета OperationTask, который идет внутри RoiInstructions,
// а тот уже внутри OperationsInstructions.

// ПРИМЕЧАНИЕ 2: Все строки закодированы в кодировке UTF-8.

// Уровень привилегий клиентов Вереска
// ППРД: не применимо
enum PrivLevel
{
	// Администратор. Может добавлять, просматривать и удалять задания, поставленные всеми операторами,
	// получать события от всех заданий. В каждый момент времени может существовать только одна 
	// сессия с уровнем доступа PL_ADMIN.
	PL_ADMIN = 0;

	// Оператор. Может добавлять, просматривать и удалять задания и получать события от их 
	// выполнения при условии, что задания были поставлены оператором с тем же маркером доступа.
	PL_OPERATOR = 1;

    // Наблюдатель. Может получать события от выполнений заданий, поставленных оператором 
	// с тем же маркером доступа, что и у наблюдателя. Событий от заданий, поставленных кем-то, 
	// у кого другой маркер доступа, равно как и самих заданий, наблюдатель не видит.
	PL_VIEWER = 2;

    // Супер-наблюдатель - наблюдатель всех заданий. Видит все задания и может получать события от 
	// выполнения всех заданий, поставленных всеми операторами вне зависимости от их маркеров доступа.
	PL_SUPER_VIEWER = 3;
};

// Описание числовых идентификаторов версий протокола
enum ProtoVersion
{
	PT_0 = 0;					// Версия, соответствующая релизу Вереска с возможностью управления через Veresk.proto
};

// Коды возможностей (features) сервера Вереска
enum FeatureCode
{
	FC_DEFAULT = 0;
};

// Работа клиента с сервером (Вереском) начинается со "здравствуйте"
// ППРД: не применимо
message HelloRequest
{
	// Представляем, как себя величать. Здесь подразумевается наименование софта, 
	// оно должно быть точным и повторяющимся от подключения к подключению.
	required string client_title = 1;

	// Запрашиваемый уровень привилегий для тех конфигураций, когда Вереск будет работать с проверкой уровня доступа.
	// Для конфигураций без проверки доступа нужно оставить значение по умолчанию, в этом случае все клиенты Вереска 
	// будут наделены одинаковыми правами PL_ADMIN.
	optional PrivLevel priv_level = 2 [default = PL_ADMIN];

	// Маркер доступа для тех конфигураций, когда Вереск будет эти маркеры проверять, иначе допустимо значение 
	// по умолчанию - просто пустая строка. Маркер доступа является аналогом комбинации "логин + пароль" и 
	// выделяется на этапе первоначальной настройки Вереска. Допускается одновременное существование нескольких
	// сессий с одинаковым маркером доступа и как одинаковым, так и разными уровнями доступа (кроме уровня
	// доступа PL_ADMIN, для которого единовременно допускается только одна сессия) при условии, что запрошенные
	// уровни доступа для данного маркера были разрешены на этапе первоначальной настройки Вереска.
	optional string access_token = 3;

	// Версия протокола, поддерживаемая клиентом. Сервер Вереска имеет право отказать в обслуживании, если версия
	// слишком старая или новее, чем та, на которую он рассчитан. Версия с номером 0 - первая после релиза поддержки
	// управления с помощью Veresk.proto. Все более поздние версии будут иметь бОльший номер.
	optional ProtoVersion client_proto_version = 4 [ default = PT_0];

	// Набор возможностей, требуемых клиентом для работы. В случае, если хотя бы одна из возможностей 
	// не поддерживается со стороны сервера, сервер вернет ошибку.
	repeated FeatureCode client_required_features = 5;
};

// В ответ на HelloRequest приходит HelloResponse
// ППРД: не применимо
message HelloResponse
{
	// Коды ответа на приветствие
	enum HelloResponseCode
	{
		HRC_OK = 0;                    // все хорошо
		HRC_PRIV_LEVEL_DENIED = 1;     // запрашиваемый уровень привилегий недопустим для этого маркера доступа
		HRC_INVALID_ACCESS_TOKEN = 2;  // неверный маркер доступа
		HRC_PROTO_VERSION_TOO_LOW = 3; // Версия протокола со стороны клиента слишком старая, клиенту нужно обновление
		HRC_PROTO_VERSION_TOO_HIGH = 4;// Версия протокола со стороны клиента выше версии сервера, при этом сервер
		                               // настроен так, что не допускает работы с более новой версией протокола.
									   // Клиент в таком случае, если способен, может еще раз послать HelloRequest
									   // с версией протокола, соответствующей серверной версии. Разумеется, при этом
									   // клиенту нужно будет в точности следовать именно этой уменьшенной версии.
		HRC_FEATURE_NOT_SUPPORTED = 5; // Как минимум одна из возможностей, требуемых клиентом, не поддерживается
								  	   // со стороны сервера.
	};

	// Код ответа на приветствие
	optional HelloResponseCode hello_response = 1 [default = HRC_OK];

	// Ответ на приветствие в виде строки (можно записать его в журнал, но оператору показывать необязательно)
	optional string message_string = 2;

	// Версия протокола сервера
	optional ProtoVersion server_proto_version = 3 [default = PT_0];

	// Возможности (features) сервера
	repeated FeatureCode server_features = 4;
};

// Это список кодов всех возможных в данном протоколе сообщений
enum MessageCode
{
	MC_HelloRequest = 0;
	MC_HelloResponse = 1;
	MC_AddTaskListRequest = 2;
	MC_AddTaskListResponse = 3;
	MC_RemoveTaskListRequest = 4;
	MC_RemoveTaskListResponse = 5;
	MC_ViewTasksRequest = 6;
	MC_ViewTasksResponse = 7;
	MC_SubscribedEventsControlRequest = 8;
	MC_SubscribedEventsControlResponse = 9;
	MC_TaskResultNotification = 10;
	MC_ProcessedFileDataRequest = 11;
	MC_ProcessedFileData = 12;
};

// !!!
// Важнейшая часть протокола - message Task и сопутствующие определения - описана в файле Veresk_common.proto
// !!!

// Запрос на добавление списка из одного или нескольких заданий
// ППРД: не применимо, используем OperationsInstructions
message AddTaskListRequest
{
	// Описание добавляемых заданий
	repeated Task tasks = 1;
};

// Ответ на добавление одного или нескольких заданий
// ППРД: не применимо - для доклада о готовности к выполнению задания используется OperationsInstructionsReport
message AddTaskListResponse
{
	// Коды ответа на запрос добавления задания
	enum AddTaskResponseCode
	{
		ATRC_OK = 0;                  // все хорошо
		ATRC_ALREADY_EXIST = 1;       // задание с таким именем уже существует
		ATRC_PRIV_LEVEL_TOO_LOW = 2;  // уровень привилегий не позволяет добавить задание
		ATRC_LOW_RESOURCES = 3;       // ресурсов Вереска недостаточно для выполнения задания
		ATRC_INVALID_TASK_PARAMS = 4; // какие-либо из параметров задания содержат противоречие, 
		                              // или недопустимые значения. Более подробную информацию 
									  // Вереск предоставит в поле message_string ответа на 
									  // запрос добавления задания.
	};

	// Ответ на запрос добавления одного задания
	message AddTaskResponse
	{
		// Имя задания
		required string task_name = 1;

		// Код ответа на добавление этого задания
		optional AddTaskResponseCode response_code = 2 [default = ATRC_OK];

		// Строка в текстовом виде с описанием ошибки, если она произошла
		optional string message_string = 3;

		// Количество узкополосных каналов, которые Вереск смог выделить на выполнение задания
		// (как сказано выше, оно может отличаться от запрашиваемого, и вообще в процессе выполнения 
		// задания может измениться). 0 будет означать, что для выполнения задания нет необходимости 
		// выделять дополнительные каналы (например, другой клиент уже поставил схожее задание).
		optional uint32 allocated_channels = 4 [default = 0];
	};

	// Ответы на запрос добавления списка заданий
	// (порядок соответствует списку добавляемых заданий в запросе на добавление)
	repeated AddTaskResponse add_task_responses = 1;
};

// Запрос на удаление списка заданий.
// Следует отметить, что задание с временем выполнения TR_FULLTIME (постоянное) удалять НЕ надо - 
// Вереск автоматически их удалит из списка сразу после того, как его выполнение завершится 
// (например, после отсылки результата их выполнения клиенту, его поставившему)
// ППРД: не применимо, используем OperationsInstructions::Cancel
message RemoveTaskListRequest
{
	// Список имен удаляемых заданий
	repeated string task_names_to_remove = 1;
};

// Ответ на запрос на удаление списка заданий
// ППРД: не применимо
message RemoveTaskListResponse
{
	// Коды ответа на запрос удаления задания
	enum RemoveTaskResponseCode
	{
		RTRC_OK = 0;                  // все хорошо
		RTRC_NOT_FOUND = 1;           // задание с таким именем не существует
		RTRC_PRIV_LEVEL_TOO_LOW = 2;  // уровень доступа недостаточен для удаления задания
		                              // Это может быть, если клиент не имеет уровня доступа 
									  // администратора или оператора и пытается удалить задание, 
									  // которое он не добавлял (например, при уровне его доступа PL_VIEWER)
	};

	// Ответ на запрос на удаление задания
	message RemoveTaskResponse
	{
		required string task_name = 1;                                         // имя задания
		optional RemoveTaskResponseCode response_code = 2 [default = RTRC_OK]; // код ответа на удаление задания
		optional string message_string = 3;                                    // строка в текстовом виде с описанием
                                                                               // ошибки, если она произошла.
	};

	// Ответы на запрос на удаление списка заданий
	// (порядок соответствует списку удаляемых заданий в запросе на удаление)
	repeated RemoveTaskResponse remove_task_responses = 1;
};

// Запрос на просмотр списка заданий, выполняемых Вереском
// ППРД: не применимо
message ViewTasksRequest
{
	// Список заданий, которые хотелось бы просмотреть. Если этот список пустой, то возвращаются все выполняемые
	// задания с учетом уровня доступа запрашивающего (для уровней PL_ADMIN и PL_SUPER_VIEWER возвращаются вообще
	// все существующие задания)
	repeated string task_names_to_view = 1;
};

// Информация об обнаруженном сигнале
// ППРД: не применимо
message DetectedSignalInfo
{
	// Тип обнаруженного сигнала
	required SignalClass sig_class = 1;
	
	// Сработал ли энергетический обнаружитель перед обнаружителем типа сигнала?
	optional bool is_energy_fired = 2 [default = false];

	// Частота настройки РПУ, при которой сработал обнаружитель (необязательно точно соответствует несущей сигнала,
	// см. ниже)
	required uint32 receiver_freq = 3;

	// Частота, которая была получена в результате коррекции того смещения, 
	// которое было получено из данных обнаружителя и после отработки отката (если откат доступен)
	// Именно эта частота является той, на которую был в итоге перестроен РПУ 
	// для записи сигнала, и именно ее можно показать оператору в качестве той, 
	// которая есть самая истинная. Для заданий типа TT_MONITORING, при выполнении которых коррекции частоты 
	// не производится, эта частота всегда будет равна receiver_freq.
	required uint32 corrected_freq = 4;

	// Отметка времени, соответствующая моменту обнаружения сигнала
	required uint64 detection_timestamp = 5;

	// Тип детектирования в узкополосном канале, при котором был обнаружен сигнал
	optional NarrowBandDetType narrow_band_det_type = 6 [ default = NBDT_USB ];

	// Нормированное пиковое значение на выходе основного обнаружителя (та величина, которая превысила 
	// порог срабатывания обнаружителя, что и послужило причиной его срабатывания и начала сеанса записи сигнала).
	// Чем больше отклонение данной величины в бОльшую сторону от порога срабатывания, тем больше шансов на то, 
	// что обнаруженный сигнал - хорошего качества.
	required float detector_peak_value = 7;

	// Порог срабатывания для основного обнаружителя, который сработал в начале сеанса записи сигнала.
	// Возможно, что это значение в будушем сможет корректироваться относительно того, что было задано в параметрах
	// задания.
	required float detector_threshold = 8;

	// Обобщенная мера достоверности на выходе энергетического обнаружителя (та величина, которая превысила 
	// порог срабатывания энергетического обнаружителя, что послужило причиной для включения основного обнаружителя).
	// Чем больше отклонение данной величины в бОльшую сторону от порога срабатывания, тем больше шансов на то, 
	// что обнаруженный сигнал - хорошего качества. Значение 0.0 указывает на то, что энергетический обнаружитель был
	// неактивен.
	optional float energy_measure = 9 [default = 0.0];

	// Порог срабатывания для энергетического обнаружителя. Значение 0.0 указывает на то, что энергетический
	// обнаружитель был неактивен. Возможно, что это значение в будушем сможет корректироваться относительно того,
	// что было задано в параметрах задания.
	optional float energy_threshold = 10 [default = 0.0];
};

// Информация о пропадании обнаруженного ранее сигнала
// ППРД: не применимо
message LostSignalInfo
{
	// Тип сигнала, который сейчас пропал из эфира
	required SignalClass lost_sig_class = 1;
	
	// Частота, на которую был настроен РПУ для записи сигнала, который сейчас пропал из эфира.
	// Соответствует частоте DetectedSignalInfo.corrected_freq.
	required uint32 recording_freq = 2;

	// Отметка времени, соответствующая моменту пропадания сигнала из эфира.
	required uint64 lost_timestamp = 3;

	// Обобщенная мера достоверности на выходе энергетического обнаружителя - та величина, которая стала сильно ниже, 
	// чем соответствуюшая ей величина в момент обнаружения сигнала (см. ниже поле energy_start_measure), что и 
	// послужило причиной думать о том, что сигнал пропал из эфира.
	// Значение 0.0 указывает на то, что энергетический обнаружитель был неактивен.
	optional float energy_measure = 4 [default = 0.0];

	// Порог срабатывания для энергетического обнаружителя. Значение 0.0 указывает на то, что энергетический
	// обнаружитель был неактивен. Возможно, что это значение в будушем сможет корректироваться относительно того,
	// что было задано в параметрах задания.
	optional float energy_threshold = 5 [default = 0.0];

	// Обобщенная мера достоверности на выходе энергетического обнаружителя в тот момент, когда сработал основной
	// обнаружитель (та величина, которая превысила порог срабатывания энергетического обнаружителя, что послужило
	// причиной для включения основного обнаружителя). Значение 0.0 указывает на то, что энергетический обнаружитель
	// был неактивен.
	optional float energy_start_measure = 6 [default = 0.0];
};

// Информация о получении результатов пеленгования/местоопределения
// ППРД: не применимо
message DirectionLocationInfo
{
	// Частота, посланная в пеленгатор
	required uint32 freq = 1;

	// Ширина полосы сигнала
	required uint32 bandwidth = 2;

	// Временная отметка, соответствующая времени посылки запроса на пеленгование/местоопределение
	required uint64 request_timestamp = 3;

	// Временная отметка, соответствующая времени получения результата пеленгования/местоопределения
	required uint64 response_timestamp = 4;

	// Результат пеленгования (в градусах)
	optional float direction = 5;

	// Результат местоопределения (GPS-координаты)
	optional string location = 6;
};

// Информация о сессии записи сигнала
// ППРД: не применимо
message RecordingSessionInfo
{
	// Информация о всех фактах обнаружения сигнала в этой сессии записи
	repeated DetectedSignalInfo det_signal_infos = 1;

	// Информация о всех фактах пропадания сигнала в этой сессии записи. Сопоставление с фактами обнаружения сигнала 
	// необходимо проводить, основываясь на упорядочивании полей detection_timestamp/lost_timestamp.
	repeated LostSignalInfo lost_signal_infos = 2;

	// Информация о данных пеленгования/местоопределения
	repeated DirectionLocationInfo dir_loc_infos = 3;
	
	// Окончена ли сессия записи сигнала на текущий момент?
	required bool is_finished = 4;

	// Продолжительнось сеанса (в секундах) на текущий момент (может изменяться с 
	// течением времени у неоконченного сеанса)
	required uint64 duration = 5;

	// Идентификатор сессии в БД Вереска. Нулевое значение означает, что запись в БД Вереска не ведется.
	optional uint32 db_session_id = 6 [default = 0];
};

// Ответ на запрос просмотра списка заданий (будет еще изменяться)
// ППРД: не применимо
message ViewTasksResponse
{
	// Информация о выполняющемся задании
	message RunningTaskInfo
	{
		// Имя задания
		required string task_name = 1;

		// Имя клиента, создавшего задание
		required string task_owner_name = 2;

		// Общее количество завершенных сессий записи сигналов
		required uint32 total_sessions_count = 3;

		// Текущие (неоконченные) сессии записи сигнала
		repeated RecordingSessionInfo current_sessions = 4;

		// Оконченные сессии записи сигнала
		repeated RecordingSessionInfo finished_sessions = 5;

		// Идентификатор задания в БД Вереска. Нулевое значение означает, что запись в БД Вереска не ведется.
		optional uint32 db_task_id = 6 [default = 0];
	};

	// Список выполняемых заданий
	repeated Task tasks = 1;

	// Информация времени выполнения о заданиях (порядок соответствует порядку в поле tasks)
	repeated RunningTaskInfo tasks_info = 2;
};

// Сообщение о добавлении или удалении подписки на уведомления.
// Для того, чтобы иметь возможность получать уведомления о происходящих событиях, необходимо послать 
// соответствующее сообщение - запрос подписки. По умолчанию уведомления о событиях высылаются только 
// на те задания, которые были добавлены в текущем сеансе работы с Вереском. При закрытии соединения 
// с Вереском и повторном его открытии необходимо заново подписаться на все уведомления. Подписка таким 
// образом разграничивается по сеансам, а не по маркерам доступа. Для удаления подписки необходимо 
// послать сообщение со сброшенным флагом subscribe
// ППРД: не применимо
message SubscribedEventsControlRequest
{
	// Имя задания, на события которого нужно подписаться. Пустая строка обозначает все задания, 
	// добавленные в сеансах с тем же маркером доступа, что и текущий. Значение "*" обозначает вообще все 
	// задания, поставленные в сеансах с любыми маркерами доступа, и работает только при наличии 
	// уровней доступа PL_ADMIN или PL_SUPER_VIEWER. Если же такого уровня доступа нет, то "*" 
	// будет эквивалентно пустой строке.
	optional string task_name = 1 [default = ""];

	// Добавить или удалить подписку?
	// true - добавить, false - удалить.
	required bool subscribe = 2;

	// Список из кодов тех сообщений, на которые мы подписываемся/отписываемся. Пустой список предписывает
	// подписаться/отписаться на/от всех видов уведомлений. В данном списке допустимы следующие коды:
	// - MC_AddTaskListResponse (работает только при пустом task_name или же равном "*")
	// - MC_RemoveTaskListResponse
	// - MC_TaskResultNotification
	repeated MessageCode message_codes = 3;

	// Если в списке message_codes есть код MC_TaskResultNotification, то тогда можно выбрать, на какие 
	// именно результаты выполнения задания можно подписаться, перечислив коды необходимых типов здесь.
	repeated TaskResultType task_result_types = 4;
};

// Ответ на запрос подписки/отписки
// ППРД: не применимо
message SubscribedEventsControlResponse
{
	// Имя задания, указанное в SubscribedEventsControlRequest
	optional string task_name = 1;

	// Список из кодов тех сообщений, на которые клиент в данный момент подписан. Если в запросе не было 
	// указано точное имя задания, то в этом списке будут все возможные коды сообщений от всех заданий.
	repeated MessageCode message_codes = 2;
};

// Запрос обработанного файла или записи сигнала от Вереска
// ППРД: не применимо (используется механизм FileRequest/FileFragment)
message ProcessedFileDataRequest
{
	// Идентификатор файла в БД Вереска
	required uint32 db_blob_id = 1 [default = 0];
};

// Данные обработанного файла или записи сигнала (ответ на запрос)
// ППРД: не применимо (используется механизм FileRequest/FileFragment)
message ProcessedFileData
{
	// Собственно, сами обработанные данные. Размер каждого элемента этого списка составляет не более мегабайта.
	required bytes payload = 1;

	// Если исходный размер данных был более мегабайта, то данные будут разбиты на несколько кусков, каждый будет 
	// передаваться в отдельном сообщении ProcessedFileData. Для их нумерации вводятся два нижеследующих поля: 
	// общее количество кусков и номер куска, которому соответствует текущее сообщение.
	optional uint32 overall_chunk_count = 2 [default = 1];
	optional uint32 current_chunk_index = 3 [default = 1];

	// Идентификатор файла с обработанными данными в БД Вереска
	optional uint32 db_blob_id = 4 [default = 0];
};

// Типы результатов выполнения задания
enum TaskResultType
{
    TRT_DetectedSignalInfo = 0;      // обнаружен сигнал, поле - detected_signal_info
	TRT_LostSignalInfo = 1;          // потерян сигнал, поле - lost_signal_info
	TRT_NewRecordingSessionInfo = 2; // новая сессия записи сигнала, поле - recording_session_info
	TRT_EndRecordingSessionInfo = 3; // конец сессии записи сигнала, поле - recording_session_info 
	TRT_ProcessedSessionInfo = 4;    // обработанные данные сессии записи сигнала, поле - processed_session_info
	TRT_DirectionLocationInfo = 5;   // результаты пеленгования/местоопределения, поле - dir_loc_info
};

// Обобщенное уведомление о результате выполнения задания
// ППРД: не применимо
message TaskResultNotification
{
	// Тип результата
	required TaskResultType task_result_type = 1;

	// Имя задания, к которому относится результат
	required string task_name = 2;

	// Далее идут поля, соответствующие разным типам результатов
	optional DetectedSignalInfo   detected_signal_info = 3;
	optional LostSignalInfo       lost_signal_info = 4;
	optional RecordingSessionInfo recording_session_info = 5;
	optional ProcessedFileMetadata        processed_metadata = 6;
	optional ProcessedFileData    processed_data = 7;
	optional DirectionLocationInfo        dir_loc_info = 8;
}

// Сетевой протокол будет такой:
// 1. uint32 - длина пакета, включая uint32 из пункта 2 с типом и тело самого сообщения
// 2. uint32 - тип сообщения (как в enum MessageCode)
// 3. собственно само сообщение
