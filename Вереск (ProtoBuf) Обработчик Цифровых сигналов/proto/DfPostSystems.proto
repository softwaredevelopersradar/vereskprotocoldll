syntax = "proto2";

option optimize_for = LITE_RUNTIME;

import "UavIdentity.proto";
import "GeoData.proto";
import "DfPostCommon.proto";
import "DfConnection.proto";

package Rdmp.DfPost;

message DfACARSRes
{
	/// уникальный идентификатор сообщения
	optional Uuid packetGUID = 1;
	/// точное время приёма сообщения
	optional FineTime receivedTime = 2;
	/// количество найденных ошибок при декодировании
	optional int32 errorsCount = 3;
	/// уровень амплитуды сигнала в дБ, не нормированный и не калиброванный
	optional double signalLvl = 4;
	/// режим работы системы ACARS, один символ
	optional string mode = 5;
	/// адрес регистрации воздушного судна
	optional string registration = 6;
	/// признак типа сообщения: данные/подтверждение
	optional bool isACK = 7;
	/// метка сообщения
	optional string msgLabel = 8;
	/// идентификатор блока сообщений
	optional int32 blockID = 9;
	/// В сообщении присутствует метка начала текста
	optional bool hasSOT = 10;
	/// В сообщении присутствует метка конца текста
	optional bool hasEOT = 11;
	/// идентификатор сообщения
	optional string msgIndex = 12;
	/// номер рейса
	optional string flightID = 13;
	/// произвольный текст, максимум 255 символов
	optional string msgText = 14;
};

message DfGmdssDscUhfRes
{
	/// Форматы сообщений системы GMDSS-DSC
	enum GmdssMsgFormat
	{
		/// запрос к географической области
		MF_GEOAREA = 0;
		/// бедствие
		MF_DISTRESS = 1;
		/// абоненты с общим интересом
		MF_COMMON_INTEREST = 2;
		/// все абоненты
		MF_ALL_SHIPS = 3;
		/// автоматический режим обращения к конкретной станции
		MF_SELECT_STATION = 4;
		/// полуавтоматический режим обращения к конкретной станции
		MF_SEMIAUTO_SEL_STA = 5;
		/// невозможный вариант: ОШИБКА
		MF_ERROR = 6;
	};
	
	/// Категории сообщений системы GMDSS-DSC
	enum GmdssMsgCategory 
	{
		/// обычное
		MC_ROUTINE = 0;
		/// безопасность
		MC_SAFETY = 1;
		/// срочное
		MC_URGENT = 2;
		/// бедствие
		MC_DISTRESS = 3;
		/// невозможный вариант: ОШИБКА
		MC_ERROR = 4;
	};
	
	/// Типы сообщений системы GMDSS-DSC
	enum GmdssMsgType
	{
		/// общий
		MT_GENERAL = 0;
		/// запрос
		MT_REQUEST = 1;
		/// ответ
		MT_ACK = 2;
		/// невозможный вариант: ОШИБКА
		MT_ERROR = 3;
	}
	
	/// Характер бедствия
	enum GmdssDistrType
	{
		/// пожар, взрыв
		DT_FIRE = 0;
		/// наводнение
		DT_FLOOD = 1;
		/// столкновение
		DT_COLLISION = 2;
		/// посадка на мель
		DT_GROUNDING = 3;
		/// крен, риск опрокидывания
		DT_BANK = 4;
		/// потопление
		DT_SINKING = 5;
		/// выведен из строя, дрейфует
		DT_ADRIFT = 6;
		/// не входит в классификацию
		DT_UNDESIGNATED = 7;
		/// команда покидает судно
		DT_ABANDONING = 8;
		/// пираты, вооруженное ограбление
		DT_PIRATES = 9;
		/// человек за бортом
		DT_MAN_OVERBOARD = 10;
		/// сообщение EPIRB
		DT_EPIRB_EMISSION = 11;
		/// невозможный вариант: ОШИБКА
		DT_ERROR = 12;
	};
	
	/// координаты и время, когда они были верны
	message GmdssTimeCoords
	{
		/// координаты, тип GeoCoordinates
		optional Point coords = 1;
		/// время, когда были верны координаты, часы
		optional int32 timeHours = 2;
		/// время, когда были верны координаты, часы
		optional int32 timeMinutes = 3;
	};
	
	/// первая команда 
	enum GmdssFstCmd
	{
		/// связь F3E/G3E, все режимы
		FST_CMD_F3E_AllModes = 0;
		/// связь F3E/G3E дуплексный режим
		FST_CMD_F3E_DUPLEX = 1;
		/// опрос
		FST_CMD_POLLING = 2;
		/// невозможно выполнить
		FST_CMD_UNABLE_TO_COMPLY = 3;
		/// конец вызова
		FST_CMD_ONE_OF_CALL = 4;
		/// передача данных
		FST_CMD_DATA = 5;
		/// связь J3E
		FST_CMD_J3E = 6;
		/// подтверждение бедствия
		FST_CMD_DISTR_ACK = 7;
		/// ретрансляция сигнала бедствия
		FST_CMD_DISTR_RELAY = 8;
		/// связь F1B/J2E TTY FEC
		FST_CMD_F1B_J2E_TTY_FEC = 9;
		/// связь F1B/J2E TTY ARQ
		FST_CMD_F1B_J2E_TTY_ARQ = 10;
		/// испытание
		FST_CMD_TEST = 11;
		/// обновление информации о местоположении судна или месте регистрации
		FST_CMD_INFO_UPDATE = 12;
		/// нет информации
		FST_CMD_NO_INFO = 13;
		/// невозможный вариант: ОШИБКА
		FST_CMD_ERROR = 14;
	};
	
	/// вторая команда
	enum GmdssSndCmd
	{
		/// причина не указана
		SND_CMD_NOREASON = 0;
		/// перегрузка на морском коммутационном центре
		SND_CMD_PATH_OVERLOAD = 1;
		/// занято
		SND_CMD_BUSY = 2;
		/// указание очереди
		SND_CMD_QUEUE_INDICATION = 3;
		/// станция выключена
		SND_CMD_STATION_OFF = 4;
		/// оператор недоступен
		SND_CMD_NO_OPERATOR = 5;
		/// оператор временно недоступен
		SND_CMD_TEMP_NO_OPER = 6;
		/// оборудование выведено из строя
		SND_CMD_EQUIPMENT_DISABLED = 7;
		/// невозможно использовать предлагаемый канал
		SND_CMD_UNSUPPORTED_CHANNEL = 8;
		/// невозможно использовать предлагаемый режим
		SND_CMD_UNSUPPORTED_MODE = 9;
		/// суда и летательные аппараты государств, не участвующих в вооруженном конфликте
		SND_CMD_GOOD_SHIPS = 10;
		/// санитарно-транспортные средства
		SND_CMD_MDEICAL_TRANSPORT = 11;
		/// платный телефон/телефонная служба общего пользования
		SND_CMD_PAY_PHONE = 12;
		/// факс/данные, согласно Рекомендации МСЭ-R M.108
		SND_CMD_DATA_ITU_M1081 = 13;
		/// нет информации
		SND_CMD_NO_INFO = 14;
		/// невозможный вариант: ОШИБКА
		SND_CMD_ERROR = 15;
	};
	
	/// идентификатор судная или станции
	message GmdssID
	{
		/// ID судна, форматированная строка
		optional string shipID = 1;
	};
	
	/// уникальный идентификатор сообщения
	optional Uuid packetGUID = 1;
	/// точное время приёма сообщения
	optional FineTime receivedTime = 2;
	/// коэффициент корреляции преамбулы, значение от 0 до 1
	optional double corrValue = 3;
	/// формат сообщения
	optional GmdssMsgFormat msgFormat = 4;
	/// категория сообщения
	optional GmdssMsgCategory msgCategory = 5;
	/// тип сообщения
	optional GmdssMsgType msgType = 6;
	/// характер бедствия
	optional GmdssDistrType distrType = 7;
	/// тип дальнейшей связи (только команды про связь)
	optional GmdssFstCmd subSeqComm = 8;
	/// первая команда
	optional GmdssFstCmd fstCmd = 9;
	/// вторая команда
	optional GmdssSndCmd sndCmd = 10;
	/// самоопознавание
	optional GmdssID selfID = 11;
	/// идентификатор адресата сообщения
	optional GmdssID destID = 12;
	/// идентификатор терпящего бедствие
	optional GmdssID distrID = 13;
	/// координаты бедствия с временной меткой
	optional GmdssTimeCoords distrCoords = 14;
	/// собственные координаты с временной меткой
	optional GmdssTimeCoords selfCoords = 15;
	/// координаты географической области, 4 вершины прямоугольной области
	repeated Point geoAreaCoords = 16;
	/// первая частота связи, Гц
	optional uint64 freq1 = 17;
	/// вторая частота связи, Гц
	optional uint64 freq2 = 18;
	/// номер первого канала связи
	optional int32 chan1 = 19;
	/// номер второго канала связи
	optional int32 chan2 = 20;
	/// продолжительность вызова
	optional int32 callDuration = 21;
	/// телефонный номер, до 20 символов
	optional string phoneNumber = 22;
};

/// сообщение системы VDL-2
message DfVDLm2Res
{
	/// тип радиостанции
	enum Vdl2StationType
	{
		/// зарезервировано для применения в будущем
		ST_RESERVED = 0;
		/// воздушное судно
		ST_AIRCRAFT = 1;
		/// назменая станция, адрес выдан ICAO
		ST_GROUND_ICAO_ADMIN = 2;
		/// наземная станция, адрес из диапазона, делегированного кому-то другому, не ICAO
		ST_GROUND_ICAO_DEL = 3;
		/// все станции
		ST_ALL = 4;
		/// невозможный вариант: ОШИБКА
		ST_ERROR = 5;
	};

	/// тип пакета данных
	enum Vdl2PacketType
	{
		/// информация
		PT_INFO = 0;
		/// управляющий
		PT_SUPERVISORY = 1;
		/// без нумерации
		PT_UNNUMBERED = 2;
		/// невозможный вариант: ОШИБКА
		PT_ERROR = 3;	
	};
	
	/// уникальный идентификатор сообщения
	optional Uuid packetGUID = 1;
	/// точное время приёма сообщения
	optional FineTime receivedTime = 2;
	/// коэффициент корреляции преамбулы, значение от 0 до 1
	optional double corrValue = 3;
	/// устранённая частотная отстройка, Гц
	optional int64 freqShift = 4;
	/// количество ошибок в преамбуле
	optional int32 syncBER = 5;
	/// длина пакета в битах
	optional int32 packetSize = 6;
	/// количество исправленных ошибок в коде Рида-Соломона 
	optional int32 errors_RS = 7;
	/// признак корректности контрольной суммы после RS-декодера
	optional bool goodCRC = 8;
	/// признак сообщения: отправитель является воздушным судном
	optional bool srcIsAirbone = 9;
	/// признак сообщения: является командой (иначе подтверждение) 
	optional bool msgIsCmd = 10;
	/// тип станции назначения, смотри описание Vdl2StationType
	optional Vdl2StationType destType = 11;
	/// ICAO-адрес станции назначения, 6 символов в HEX: "2A3197"
	optional string destAddr = 12;
	/// тип станции отправителя, смотри описание Vdl2StationType
	optional Vdl2StationType srcType = 13;
	/// ICAO-адрес станции отправителя, 6 символов в HEX: "2A3197"
	optional string srcAddr = 14;
	/// тип пакета (информация, управляющий, ненумерованный)
	optional Vdl2PacketType packType = 15;
	/// номер посылаемой последовательности
	optional uint32 seqSndNumb = 16;
	///номер принятой последовательности
	optional uint32 seqRcvNumb = 17;
	/// признак последнего сообщения
	optional bool isFinal = 18;
	/// массив бит, пользовательская информация
	optional bytes userInfo = 19;
	/// если сообщение VDL-2 содержит внутри себя сообщение ACARS, здесь будет ссылка
	optional Uuid AcarsRef = 20;
};

message DfACARSResTable
{
	/// транспортная информация
	optional DfTransporterInfo tranposterInfo = 1;
	/// данные демодуляции
	repeated DfACARSRes data = 2;
};

message DfGMDSSDscResTable
{
	/// транспортная информация
	optional DfTransporterInfo tranposterInfo = 1;
	/// данные демодуляции
	repeated DfGmdssDscUhfRes data = 2;
};

message DfVDLm2ResTable
{
	/// транспортная информация
	optional DfTransporterInfo tranposterInfo = 1;
	/// данные демодуляции
	repeated DfVDLm2Res data = 2;
};